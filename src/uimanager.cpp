#include <uimanager.h>
#include <core.h>

int UIManager::uiElementID = 0;

UIManager::UIManager(int w, int h)
{
    UIManager::uiElementID = 0;
    uiDimension   = glm::vec2(w, h);
    //
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0); // location 0 in shaders (vertCoords)
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(1); // location 1 in shaders (texCoords)
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)(2*sizeof(GLfloat))); // after 2 elemets
    glBindVertexArray(0);

    m_uishader = new Shader(SHADERS_ROOT "ui_es.vert", SHADERS_ROOT "ui_es.frag");
}

UIManager::~UIManager()
{

}

bool UIManager::isOver(unistring item)
{
    if(m_elements.at(item))
        return m_elements.at(item)->isHovered;
    return false;
}

void UIManager::render(GLuint targetFBO)
{
    glm::mat4 ui_matrix;
    for(auto &uiobj : m_elements)
    {
        uiobj.second->update(Core::c_shared->mousePos());
        //
        glBindFramebuffer(GL_FRAMEBUFFER, targetFBO);
          glDisable(GL_DEPTH_TEST);
          glBindVertexArray(quadVAO);
              glActiveTexture(GL_TEXTURE0);
              glBindTexture(GL_TEXTURE_2D, Core::c_shared->textureID(uiobj.second->texn()));

              ui_matrix = glm::translate(glm::mat4(), glm::vec3( 2*( (uiobj.second->px()+uiobj.second->w()/2)/uiDimension.x)-1.f,
                                                                 2*( (uiDimension.y - (uiobj.second->py()+uiobj.second->h()/2) )/uiDimension.y)-1.f, 0.f));

              ui_matrix = glm::scale(ui_matrix, glm::vec3(uiobj.second->w()/uiDimension.x,
                                                          uiobj.second->h()/uiDimension.y, 1.f));

              m_uishader->use();
              m_uishader->setInt("uiTex", 0);
              m_uishader->setMat4("uimatx", ui_matrix);

              glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindTexture(GL_TEXTURE_2D, 0);
          glBindVertexArray(0);
        glEnable(GL_DEPTH_TEST);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}

unistring UIManager::addElement(UIElement *other)
{
    unistring target = "element_"+to_string(UIManager::uiElementID);
    m_elements[target] = other;
    UIManager::uiElementID++;
    return target;
}

