#include <renderer.h>
#include <core.h>

Renderer::Renderer() :
  s_time(0),
  FBO(0), finalFBO(0),
  RBD(0), RBT(0),
  fbVAO(0), fbVBO(0), fbTexID(0),
  cubemapVAO(0), cubemapVBO(0),
  scrWidth(1280), scrHeight(720),
  ltype(LIGHT_POINT)
{
  lpos = glm::vec3(0.f, 1.0f, 1.f);
  lcolor = glm::vec3(1.f, 1.f, 1.f);
  m_fontmgr = new FontManager;
}
Renderer::~Renderer()
{
  SDL_ShowCursor(SDL_ENABLE);
  SDL_SetWindowGrab(m_window, SDL_FALSE);

  SDL_GL_DeleteContext(m_glcontext);

  if(m_iout != nullptr)
    SDL_DestroyRenderer(m_iout);
  if(m_window != nullptr)
    SDL_DestroyWindow(m_window);
  //
  SDL_Quit();

  if(m_main_shader != nullptr)
    delete m_main_shader;
  if(m_outline_shader != nullptr)
    delete m_outline_shader;
  if(m_fbuff_shader != nullptr)
    delete m_fbuff_shader;
  if(m_skybox_shader != nullptr)
    delete m_skybox_shader;
  if(m_particle_shader != nullptr)
    delete m_particle_shader;
  //
  glDeleteFramebuffers(1, &FBO);
  glDeleteRenderbuffers(1, &RBT);
  glDeleteRenderbuffers(1, &RBD);
  glDeleteFramebuffers(1, &finalFBO);
  glDeleteTextures(1, &fbTexID);
  glDeleteBuffers(1, &fbVBO);
  glDeleteVertexArrays(1, &fbVAO);
  glDeleteBuffers(1, &cubemapVBO);
  glDeleteVertexArrays(1, &cubemapVAO);
  //
}

void Renderer::render(Camera *m_cam, Scene3d *scene, UIManager *uimgr)
{
  // Draw all here
  glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear FBO
    auto iter = scene->objs().begin();
    while( iter != scene->objs().end())
    {
      if(!iter->second)
      {
          ++iter;
          continue;
      }
      m_main_shader->use();
      //
      m_main_shader->setInt("light", ltype);
      m_main_shader->setInt("lightType", LIGHT_SUN);
      m_main_shader->setFloat("lightPower", 1.f);
      m_main_shader->setVec3("lightPos", lpos);
      m_main_shader->setVec3("lightColor", lcolor);
      //
      m_main_shader->setMat4("View", m_cam->matrix());
      iter->second->draw(m_main_shader);

      ++iter;
    }

    for(auto& particles : scene->particleSys())
    {
      if(!particles.second)
          continue;
      m_particle_shader->use();
      m_particle_shader->setMat4("View", m_cam->matrix());
      particles.second->draw(m_particle_shader);
    }

    glDepthFunc(GL_LEQUAL);
    glBindVertexArray(cubemapVAO);
      m_skybox_shader->use();
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_CUBE_MAP, Core::c_shared->getCubemap(0));
      m_skybox_shader->setInt("skybox", 0);
      m_skybox_shader->setMat4("View", m_cam->matrix());
      glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glDepthFunc(GL_LESS);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glBindFramebuffer(GL_READ_FRAMEBUFFER, FBO);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, finalFBO);
    glClear(GL_COLOR_BUFFER_BIT); // Clear finalFBO
    glBlitFramebuffer(0, 0, scrWidth, scrHeight, 0, 0, scrWidth, scrHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST); // Copy color data
  glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

  uimgr->render(finalFBO);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT); // Clear screen
    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(fbVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fbTexID);
        m_fbuff_shader->use();
        m_fbuff_shader->setInt("stime", s_time);
        m_fbuff_shader->setInt("renderTexture", 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
      glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
  glEnable(GL_DEPTH_TEST);
  //
  SDL_GL_SwapWindow(m_window);
  SDL_Delay(1000/TARGET_FPS); // 1000 / 60
  s_time += (1000/TARGET_FPS);
}

void Renderer::initGL()
{
  glEnable(GL_NORMALIZE);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_TEXTURE_CUBE_MAP);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW); // Which faces are 'BACK'? Counter-ClockWise ( aka CCW: 1->3->2, CW: 1->2->3)

  glShadeModel(GL_SMOOTH);

  glEnable(GL_MULTISAMPLE);

  int w,h;
  SDL_GetWindowSize(m_window, &w, &h);
  glViewport(0, 0, w, h);

  m_main_shader = new Shader(SHADERS_ROOT "main_es.vert", SHADERS_ROOT "main_es.frag");
  m_outline_shader = new Shader(SHADERS_ROOT "main_es.vert", SHADERS_ROOT "outline_es.frag");
  m_fbuff_shader = new Shader(SHADERS_ROOT "fbuff_es.vert", SHADERS_ROOT "fbuff_es.frag");
  m_skybox_shader = new Shader(SHADERS_ROOT "skybox_es.vert", SHADERS_ROOT "skybox_es.frag");
  m_particle_shader = new Shader(SHADERS_ROOT"particles_es.vert", SHADERS_ROOT"particles_es.frag");

  Logger::log("Renderer", "OpenGL init complete.");
}

void Renderer::initSDL()
{
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_TIMER) < 0)
    {
      Logger::err("Renderer", SDL_GetError());
    }
  Logger::log("Renderer", "SDL2 init complete.");

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4); // OpenGL 4.x
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3); // OpenGL x.3
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  m_window    = SDL_CreateWindow("SDL2.0 Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, DW_WIDTH, DW_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
  m_glcontext = SDL_GL_CreateContext(m_window);
  SDL_GL_MakeCurrent(m_window, m_glcontext);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24); // 24-depth buffer, reccomended 16 or 24 or 32
  SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);
  SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 1 );

  SDL_GL_SetSwapInterval(1); // VSync

//  int* major_minor = (int*)calloc(2, sizeof(int));
//  glGetIntegerv(GL_MAJOR_VERSION, &major_minor[0]);
//  glGetIntegerv(GL_MINOR_VERSION, &major_minor[1]);
//  printf("Using OpenGL %d.%d\n", major_minor[0], major_minor[1]);
//  fflush(stdout);
//  free(major_minor);

#ifndef __APPLE__
  glewExperimental = GL_TRUE;
  if(glewInit() != GLEW_OK)
    Logger::err("Renderer", "Failed to init GLEW!");
#endif
  m_iout      = SDL_CreateRenderer(m_window, -1, 0);

  if(m_window == nullptr)
    {
      Logger::err("Renderer", "SDL2 failed to create window.");
    }
}

void Renderer::setWindowTitle(unistring title)
{
  SDL_SetWindowTitle(m_window, title.c_str());
}

void Renderer::setWindowSize(glm::vec2 size)
{
    SDL_SetWindowSize(m_window, (int)size.x, (int)size.y);
}

void Renderer::setWindowFull(bool full)
{
    SDL_SetWindowFullscreen(m_window, full ? SDL_WINDOW_FULLSCREEN : 0);
}

void Renderer::grabMouse()
{
  SDL_SetWindowGrab(m_window, SDL_TRUE);
  SDL_SetRelativeMouseMode(SDL_TRUE);
}

void Renderer::releaseMouse()
{
  SDL_SetWindowGrab(m_window, SDL_FALSE);
  SDL_SetRelativeMouseMode(SDL_FALSE);
}

uint64_t Renderer::stime()
{
  return s_time;
}

void Renderer::genRenderbuffer(int w, int h, bool isDepth, GLuint &buff)
{
  if(buff == 0)
    glGenRenderbuffers(1, &buff);
  glBindRenderbuffer(GL_RENDERBUFFER, buff);
  int samples = 8; // MSAA x8
  glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, isDepth ? GL_DEPTH_COMPONENT24 : GL_RGBA16, w, h);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, isDepth ? GL_DEPTH_ATTACHMENT : GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, buff);
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

glm::vec2 Renderer::dim()
{
  return glm::vec2(scrWidth, scrHeight);
}

GLuint Renderer::gFinalFBO()
{
  return finalFBO;
}

SDL_Window *Renderer::windowPtr()
{
  return m_window;
}

void Renderer::init(int width, int height)
{
  scrWidth = width; scrHeight = height;
  initSDL();
  setWindowSize(glm::vec2(scrWidth,scrHeight));
  initGL();

  if(fbVAO == 0)
    glGenVertexArrays(1, &fbVAO);
  if(fbVBO == 0)
    glGenBuffers(1, &fbVBO);

  glBindVertexArray(fbVAO);
  glBindBuffer(GL_ARRAY_BUFFER, fbVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0); // location 0 in shaders (vertCoords)
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)0);
  glEnableVertexAttribArray(1); // location 1 in shaders (texCoords)
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)(2*sizeof(GLfloat))); // after 2 elemets
  glBindVertexArray(0);

  if(FBO == 0)
    glGenFramebuffers(1, &FBO);

  glBindFramebuffer(GL_FRAMEBUFFER, FBO); // Bind
  genRenderbuffer(scrWidth, scrHeight, false, RBT);
  genRenderbuffer(scrWidth, scrHeight, true, RBD);

  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  {
    Logger::err("Renderer", "Can't init render-FBO!");
  }

  if(finalFBO == 0)
    glGenFramebuffers(1, &finalFBO);
  if(fbTexID == 0)
    glGenTextures(1, &fbTexID);

  glBindTexture(GL_TEXTURE_2D, fbTexID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, scrWidth, scrHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, finalFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbTexID, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  ostringstream errStr;
  glBindFramebuffer(GL_FRAMEBUFFER, finalFBO);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  {
    errStr << std::hex << glCheckFramebufferStatus(GL_FRAMEBUFFER);
    Logger::err("Renderer", "Can't init postprocessing-finalFBO! Status: 0x"+unistring(errStr.str()));
    if(errStr.str() == "8cd7")
      Logger::err("Renderer", "Human-format: 0x8cd7 means GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0); // Set framebuffer to default, usually it's main window

  genSkybox();
  Logger::log("Renderer", "Off-screen framebuffer init complete.");
}

void Renderer::genSkybox()
{
  GLfloat skyboxVertices[] = {
    // positions
    -10.0f,  10.0f, -10.0f,
    -10.0f, -10.0f, -10.0f,
    10.0f, -10.0f, -10.0f,
    10.0f, -10.0f, -10.0f,
    10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,

    -10.0f, -10.0f,  10.0f,
    -10.0f, -10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f, -10.0f,
    -10.0f,  10.0f,  10.0f,
    -10.0f, -10.0f,  10.0f,

    10.0f, -10.0f, -10.0f,
    10.0f, -10.0f,  10.0f,
    10.0f,  10.0f,  10.0f,
    10.0f,  10.0f,  10.0f,
    10.0f,  10.0f, -10.0f,
    10.0f, -10.0f, -10.0f,

    -10.0f, -10.0f,  10.0f,
    -10.0f,  10.0f,  10.0f,
    10.0f,  10.0f,  10.0f,
    10.0f,  10.0f,  10.0f,
    10.0f, -10.0f,  10.0f,
    -10.0f, -10.0f,  10.0f,

    -10.0f,  10.0f, -10.0f,
    10.0f,  10.0f, -10.0f,
    10.0f,  10.0f,  10.0f,
    10.0f,  10.0f,  10.0f,
    -10.0f,  10.0f,  10.0f,
    -10.0f,  10.0f, -10.0f,

    -10.0f, -10.0f, -10.0f,
    -10.0f, -10.0f,  10.0f,
    10.0f, -10.0f, -10.0f,
    10.0f, -10.0f, -10.0f,
    -10.0f, -10.0f,  10.0f,
    10.0f, -10.0f,  10.0f
  };
  if(cubemapVAO == 0)
    glGenVertexArrays(1, &cubemapVAO);
  if(cubemapVBO == 0)
    glGenBuffers(1, &cubemapVBO);
  glBindVertexArray(cubemapVAO);
  glBindBuffer(GL_ARRAY_BUFFER, cubemapVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glBindVertexArray(0);
}

Shader *Renderer::mainShader()
{
  return m_main_shader;
}

Shader *Renderer::fbuffShader()
{
    return m_fbuff_shader;
}

void Renderer::setLight(glm::vec3 newpos, int type)
{
    lpos = newpos;
    ltype = type;
}

SDL_Renderer* Renderer::renderer()
{
    return m_iout;
}
