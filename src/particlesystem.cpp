#include <particlesystem.h>

ParticleSystem::ParticleSystem()
  : pMode(P_RANDOM), pSize(1.f), pParticleSize(1.f),
    pCount(3), pColor(glm::vec3(1.f, 0.f, 1.f)),
    pMatrix(glm::mat4(1.f))
{
  if(pVAO == 0)
    glGenVertexArrays(1, &pVAO);
  if(pVBO == 0)
    glGenBuffers(1, &pVBO);

  glBindVertexArray(pVAO);
    glBindBuffer(GL_ARRAY_BUFFER, pVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (void*)(2*sizeof(GLfloat)));
  glBindVertexArray(0);
}

void ParticleSystem::draw(Shader *shader)
{
  glBindVertexArray(pVAO);
  for(int i=0; i < pCount; i++)
  {
      if(pMode ==  P_RANDOM)
      {
        render(shader, glm::vec3(pSize-randFloat(pSize*2), pSize-randFloat(pSize*2), pSize-randFloat(pSize*2)));
      }
  }
  glBindVertexArray(0);
}

void ParticleSystem::setParticle(int pc, GLuint tid, int md, float mxs, float ps)
{
  pMode = md;
  if(mxs > 0.f)
    pSize = mxs;
  if(ps > 0.f)
    pParticleSize = ps;
  if(pc < 65536)
    pCount = pc;
  if(pTexID != 0)
    pTexID = tid;
}

glm::vec3 &ParticleSystem::color()
{
  return pColor;
}

void ParticleSystem::render(Shader *shade, glm::vec3 pos)
{
  glDisable(GL_CULL_FACE);

  pMatrix = glm::translate(glm::mat4(1.f), pos);
  pMatrix = glm::scale(pMatrix, glm::vec3(pParticleSize));
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, pTexID);
    shade->setInt("texImage", 0);
    shade->setFloat("intensity", 2.f);
    shade->setMat4("Model", pMatrix);
    glDrawArrays(GL_TRIANGLES, 0, 6); // Quad
  glBindTexture(GL_TEXTURE_2D, 0);

  glEnable(GL_CULL_FACE);
}

