#include <config.h>
#include <core.h>

void Config::cfgerr(unistring errmsg)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                             "Config load error!",
                             errmsg.c_str(),
                             nullptr);
    exit(-1);
}

void Config::loadCfg(AppConfig *conf)
{
    if(luaL_loadfile(Core::lua_shared, CFG_FILE))
    {
        Logger::err("Config", "Failed to load configuration file!");
        return;
    }
    lua_call(Core::lua_shared, 0, 0);

    lua_getglobal(Core::lua_shared, "Engine_OnGameStart");
    lua_call(Core::lua_shared, 0, 0);

    lua_getglobal(Core::lua_shared, "engineConf"); // Get table with info

    conf->app_name    = LuaString(Core::lua_shared, "title");
    conf->start_scene = LuaString(Core::lua_shared, "firstlevel");
    conf->app_width   = LuaInt(Core::lua_shared, "width");
    conf->app_height  = LuaInt(Core::lua_shared, "height");
    conf->app_isfull  = LuaInt(Core::lua_shared, "fullscreen");

    conf->start_script = LuaString(Core::lua_shared, "start_script");

    Logger::log("Config", conf->app_isfull ? "Fullscreen mode" : "Windowed mode");
}

void Config::cfgwarn(unistring warnmsg)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING,
                             "Warning in app!",
                             warnmsg.c_str(),
                             nullptr);
}
