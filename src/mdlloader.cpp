#include <mdlloader.h>

MdlLoader::MdlLoader()
{
  m_loader = new Loader3D;
}

MdlLoader::~MdlLoader()
{
    for(pair<unistring, GLuint> vao : m_VAOs)
        glDeleteVertexArrays(1, &vao.second);
    for(pair<unistring, GLuint> buff : m_buffers)
        glDeleteBuffers(1, &buff.second);

    if(m_loader != NULL)
      delete m_loader;

    vdata.clear();
    m_VAOs.clear();
    m_buffers.clear();
}

void MdlLoader::loadModel(unistring mdl, unistring alias)
{
    if(mdl.length() < 4)
    {
        Logger::err("MdlLoader", "Invalid model file name: "+mdl);
        return;
    }
    unistring extf = mdl.substr(mdl.length()-3, mdl.length()-1);
    if(extf == "dae") // Collada
        m_loader->LoadModelDAE(mdl, &vdata);
    else if(extf == "obj")
        m_loader->LoadModelOBJ(mdl, &vdata);
    else
    {
        Logger::log("MdlLoader", "Failed to load file: "+mdl+" with extension '"+extf+"'");
        return;
    }

    if(m_VAOs[alias] == 0)
        glGenVertexArrays(1, &m_VAOs[alias]);
    if(m_buffers[alias] == 0)
        glGenBuffers(1, &m_buffers[alias]);

    glBindVertexArray(m_VAOs[alias]);
        glBindBuffer(GL_ARRAY_BUFFER, m_buffers[alias]);
        glBufferData(GL_ARRAY_BUFFER, vdata.size()*sizeof(GLfloat), vdata.data(), GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(5*sizeof(GLfloat)));
    glBindVertexArray(0);

    m_sizes[alias] = vdata.size()/8; // vx,vy,vz,tx,ty,nx,ny,nz = count(8)
}

void MdlLoader::getModelInfo(unistring alias, GLuint *addr, uint *size)
{
    if( (m_VAOs.find(alias) == m_VAOs.end()) ||
        (m_buffers.find(alias) == m_buffers.end()))
    {
        fprintf(stdout, "'%s' model info won't be retrieved.\n", alias.c_str());
        fflush(stdout);
        return;
    }

    *addr = m_VAOs.at(alias);
    *size = m_sizes.at(alias);
}

GLuint MdlLoader::getVAO(unistring alias)
{
  if(m_VAOs.find(alias) != m_VAOs.end())
    return m_VAOs.at(alias);
  else
    return 0;
}
