#include <esasl_parser.h>

ESAsL_Parser::ESAsL_Parser()
{

}

void ESAsL_Parser::parseLine(unistring line, ESAsL_Script *tscript)
{
    unistrlist parts;

    // First split, detect parts separated by space
    boost::split(parts, line, is_any_of(" "), token_compress_off); // Small amount of data, won't compress

    unistring tmp;

    if(parts.size() < 2)
    {
        if(parts[0] == "exit")
            exit(0);
        else if(parts[0] == "pause")
        {
            printf("Paused game. (not really)");
            fflush(stdout);
        }
    }
    else
    {
        if(parts[0] == "print")
        {
            tmp.clear();
            for(int i=1; i < parts.size(); i++)
            {
                if(i < parts.size()-1 && i != 0)
                    tmp += parts[i] + " ";
                else
                    tmp += parts[i];
            }
            printf("%s\n", tmp.c_str());
            fflush(stdout);
            tmp.clear();
        }
        else if(parts[0] == "mov")
        {
            unistrlist argParts;
            tmp.clear();
            for(int i=1; i < parts.size(); i++)
            {
                if(i < parts.size()-1 && i != 0)
                    tmp += parts[i]+" ";
                else
                    tmp += parts[i];
            }
            boost::split(argParts, tmp, is_any_of(","), token_compress_off);
            tmp.clear();
            for(int i=1; i < argParts.size(); i++)
            {
                tmp += argParts[i];
            }

            /*@mb - math begin, @me - math end, @tb - text begin, @te - text end, @comm - commentary start position*/
            int mb=-1, me=-1, te=-1, tb=-1, comm=-1;
            bool isMath=false, isText = false;
            for(int i=0; i < tmp.size(); i++)
            {
                if(tmp[i] == '{')
                {
                    isMath = true;
                    mb = i;
                }
                else if(tmp[i] == '}')
                    me = i;

                if(tmp[i] == '[')
                {
                    isText = true;
                    tb = i;
                }
                else if(tmp[i] == ']')
                    te = i;

                if(tmp[i] == ';')
                    comm = i;
            }

            if(comm > 0)
                tmp = tmp.erase(comm, tmp.size());

            if(isMath)
            {
                if(mb > 0 && me > 0)
                {
                    tmp = tmp.erase(0, mb+1);
                    tmp = tmp.erase(me-2, tmp.size());
                    tmp = solveMath(tmp);
                }
                else
                {
                    tmp = "nil";
                }
            }
            else if(isText)
            {
                if(te > 0 && tb > 0)
                {
                    tmp = tmp.erase(0, tb+1);
                    tmp = tmp.erase(te-2, tmp.size());
                }
                else
                {
                    tmp = "nil";
                }
            }
            else // Get input from function or variable
            {
                boost::trim(argParts[0]);
                boost::trim(argParts[1]);
                tmp = tscript->variables[argParts[1]];
            }

            tscript->variables[argParts[0]] = tmp;
            tmp.clear();
        }
        else if(parts[0] == "printv")
        {
            if(tscript->variables[parts[1]].size() > 0)
                printf("%s\n", tscript->variables[parts[1]].c_str());
            else
                printf("Variable '%s' haven't been assigned!\n", parts[1].c_str());
            fflush(stdout);
        }
    }
}

unistring ESAsL_Parser::solveMath(unistring eval)
{
    unistring res;
    unistrlist parts;

    boost::split(parts, eval, is_any_of("+"), token_compress_off);
    if(parts.size() < 2)
    {
        boost::split(parts, eval, is_any_of("-"), token_compress_off);
        if(parts.size() < 2)
        {
            boost::split(parts, eval, is_any_of("*"), token_compress_off);
            if(parts.size() < 2)
            {
                boost::split(parts, eval, is_any_of("/"), token_compress_off);
                if(parts.size() < 2)
                    res = "NULL";
                else
                {
                    res = to_string( atof(parts[0].c_str()) / atof(parts[1].c_str()) );
                }
            }
            else
            {
                res = to_string( atof(parts[0].c_str()) * atof(parts[1].c_str()) );
            }
        }
        else
        {
            res = to_string( atof(parts[0].c_str()) - atof(parts[1].c_str()) );
        }
    }
    else
    {
        res = to_string( atof(parts[0].c_str()) + atof(parts[1].c_str()) );
    }
    return res;
}

ESAsL_Script *ESAsL_Parser::parseFile(unistring fname)
{
    ifstream fin;
    fin.open(fname);
    if(!fin.is_open())
    {
        Logger::err("ESAsL_Parser", "Failed to open file: "+fname);
        return NULL;
    }

    unistring fbuff;
    getline(fin, fbuff, (char)fin.eof()); // Read all file

    return parseText(fbuff);
}

ESAsL_Script *ESAsL_Parser::parseText(unistring text)
{
    ESAsL_Script *script = new ESAsL_Script;

    vector<unistring> lines;
    boost::split(lines, text, is_any_of("\n"), token_compress_on); // Split by lines

    for(unistring &line : lines)
    {
        boost::trim(line);
        parseLine(line, script);
    }
    return script;
}

void ESAsL_Script::appendFunc(ESAsL_Func* func)
{
    funcs.push_back(func);
}
