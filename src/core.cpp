#include <core.h>

Core* Core::c_shared = NULL;
lua_State* Core::lua_shared = NULL;
unistring Core::curMap = "";

Core::Core()
{
    srand(time(nullptr));
    c_shared = this;

    lua_shared = luaL_newstate();

    luaL_openlibs(lua_shared);
}

Core::~Core()
{
    cleanup();
}

void Core::cleanup()
{
    Logger::log("Core", "Started cleanup...");

    if(lua_shared != NULL)
        lua_close(lua_shared);

    if(m_appconf != NULL)
        delete m_appconf;

    if(m_mdlloader != NULL)
        delete m_mdlloader;
    if(m_tloader != NULL)
        delete m_tloader;
    if(m_maploader != NULL)
        delete m_maploader;

    if(m_uimgr != NULL)
        delete m_uimgr;
    if(m_audiomgr != NULL)
        delete m_audiomgr;
    if(m_event != NULL)
        delete m_event;
    if(m_processor != NULL)
        delete m_processor;
    if(m_netmgr != NULL)
        delete m_netmgr;
    if(m_renderer != NULL)
        delete m_renderer;
    if(m_camera != NULL)
        delete m_camera;
    if(m_gamescene != NULL)
        delete m_gamescene;
#ifdef LIBFREED
    LibFreed_Quit();
#endif
}

void Core::init(bool server)
{
    m_appconf = new AppConfig;
    Config::loadCfg(m_appconf);

    /*
    m_appconf->app_width = 1280;
    m_appconf->app_height = 720;
    m_appconf->app_isfull = false;
    */
#ifdef LIBFREED
    if(!LibFreed_IsRunning())
        Logger::warn("Core", "SteamAPI init error!");
    else
        LibFreed_Init();

    pname = unistring( LibFreed_Friends()->GetNickname() );
    Logger::info("Freed info", "Hello, "+pname+" :D !" );
#endif

    m_renderer = new Renderer;
    m_renderer->init(m_appconf->app_width, m_appconf->app_height);
    m_renderer->setWindowFull(m_appconf->app_isfull);

    m_event     = new SDL_Event;
    m_processor = new GameEvents;

    m_camera = new Camera(90.f, (float)m_appconf->app_width/(float)m_appconf->app_height);

    m_audiomgr = new AudioManager;
    m_gamescene = new Scene3d;
    m_mdlloader = new MdlLoader;
    m_tloader = new TexLoader;
    m_maploader = new MapLoader;

    m_uimgr   = new UIManager(m_appconf->app_width, m_appconf->app_height);

    m_netmgr = new NetworkManager(server);
    m_renderer->setWindowTitle(m_appconf->app_name);

    //
    register_lua_hooks();
    //

    if( luaL_loadfile(Core::lua_shared, (unistring(SCRIPTS_ROOT)+m_appconf->start_script).c_str()))
        Logger::err("Core", "Failed to load startscript.");
    else
    {
        lua_pcall(Core::lua_shared, 0, 0, 0);

        lua_getglobal(Core::lua_shared, "Engine_OnGameStart");
        lua_call(Core::lua_shared, 0, 0);
    }

    loadMaps();
    preloadData();
    clearPreloadLists();
}

int Core::exec()
{
    m_quit = false;

    loadMapToScene(m_maploader->mapPtr("testMap"));
    while(!m_quit)
    {
        m_netmgr->process();

        processEvents();
        m_camera->audioUpdate();
        m_renderer->render(m_camera, m_gamescene, m_uimgr);
        //
        if(m_processor->isMouseDown(SDL_BUTTON_LEFT))
        {
        }
    }
    return 0;
}

void Core::quit()
{
    m_quit = true;
}

void Core::loadMaps()
{
    m_tloader->loadTexture(IMG_ROOT "hammer_icon.png", "hammericon");
    m_tloader->loadTexture(IMG_ROOT "menu_icon.png", "menuicon");
    //
    m_maploader->loadMap(MAPS_ROOT"main.lm", "testMap", &m_preloadMdl, &m_preloadTex, &m_preloadSnd, &m_preloadMus);
    Core::curMap = (MAPS_ROOT "main.lm");
}

void Core::preloadData()
{
    // ==============================================================
    for(auto& ti : m_preloadTex)
    {
        m_tloader->loadTexture(unistring(IMG_ROOT)+ti.second, ti.first);
    }
    // ==============================================================
    for(auto& mi : m_preloadMdl)
    {
        m_mdlloader->loadModel(unistring(MODELS_ROOT)+mi.second, mi.first);
    }
    // ==============================================================
    for(auto& si : m_preloadSnd)
    {
        m_audiomgr->loadSound(unistring(SOUND_ROOT)+si.second, si.first);
    }
    // ==============================================================
    for(auto& musi : m_preloadMus)
    {
        m_audiomgr->loadTrack(unistring(MUSIC_ROOT)+musi.second, musi.first);
    }
}

void Core::clearPreloadLists()
{
    m_preloadMdl.clear();
    m_preloadTex.clear();
    m_preloadSnd.clear();
    m_preloadMus.clear();
}

void Core::loadMapToScene(Map3d *other)
{
    if(other == NULL)
        return;
    GLuint *md = (GLuint*)malloc(3*sizeof(GLuint));
    for(int i=0; i < other->objs()->size(); i++)
    {
        m_mdlloader->getModelInfo(other->obj(i)->mdl, &md[0], &md[1]);
        m_tloader->getTextureInfo(other->obj(i)->tex, &md[2]);

        if(other->obj(i)->logicType == "worker")
        {
            ORSGL_Worker* tmpWorker = new ORSGL_Worker;

            tmpWorker->setModel(md[0], md[1]);
            tmpWorker->setTex(md[2]);
            tmpWorker->setAlias(other->obj(i)->o_name);
            tmpWorker->setPos(other->obj(i)->o_pos);
            tmpWorker->setRotation(other->obj(i)->o_rot);
            tmpWorker->scale(other->obj(i)->o_scale);
            tmpWorker->setLogicType(other->obj(i)->logicType);
            tmpWorker->setOwner(other->obj(i)->owner);
            m_gamescene->attachObject(tmpWorker);
        }
        else
        {
            m_gamescene->addObject(md[0], md[1], md[2], other->obj(i)->o_name,
                                   false, other->obj(i)->o_pos,other->obj(i)->o_rot,
                                   other->obj(i)->o_accel,other->obj(i)->o_bounds,other->obj(i)->btype,
                                   other->obj(i)->logicType, other->obj(i)->o_scale);
        }
    }
    for(int i=0; i < other->bgMusic()->size(); i++)
    {
        m_audiomgr->playSound(other->bgMusic()->at(i), m_audiomgr->bgMusSrc());
    }
    free(md);
    m_renderer->setLight(other->getLightPos(), other->getLightType());
    m_gamescene->updateScene();
}

GLuint Core::getAnimFrame(unistring anim, uint_fast16_t frame)
{
    return m_mdlloader->getVAO(anim+"_"+to_string(frame));
}

GLuint Core::textureID(unistring texn)
{
    return m_tloader->texid(texn);
}

void Core::processEvents()
{
    while(SDL_PollEvent(m_event))
    {
        switch (m_event->type) {
            case SDL_QUIT:
                m_quit = true;
            break;
            case SDL_MOUSEBUTTONDOWN:
                if(m_event->button.button > 0)
                {
                    if(m_event->button.button == SDL_BUTTON_MIDDLE)
                        m_renderer->grabMouse();

                    m_processor->button_clicked[m_event->button.button-1] = true;
                    m_processor->button_down[m_event->button.button-1] = true;
                }
            break;
            case SDL_MOUSEBUTTONUP:
                if(m_event->button.button > 0)
                {
                    //
                    if(m_event->button.button == SDL_BUTTON_MIDDLE)
                        m_renderer->releaseMouse();
                    //
                    m_processor->button_down[m_event->button.button-1] = false;
                }
            break;
            case SDL_MOUSEWHEEL:
            if(m_event->wheel.y > 0.f)
                m_camera->move(glm::vec3(0.f, -SCROLL_SPEED, 0.f)*DELTATIME);
            else if(m_event->wheel.y < 0.f)
                m_camera->move(glm::vec3(0.f, SCROLL_SPEED, 0.f)*DELTATIME);
            break;
            case SDL_MOUSEMOTION:
            break;
            default:
            break;
        }
    }

    if(!m_processor->isMouseDown(SDL_BUTTON_MIDDLE))
    {
        glm::vec3 dir(0.f);
        if(m_processor->mouseRect(OUTSIDE, glm::vec2(SCROLL_BORDER, SCROLL_BORDER),
                                  glm::vec2(m_renderer->dim().x-(SCROLL_BORDER*2), m_renderer->dim().y-(SCROLL_BORDER*2))))
        {
            glm::vec2 finalVector = m_renderer->dim() - m_processor->mousePos();
            if(finalVector.x <= SCROLL_BORDER) // Right
                dir.x = 1.f;
            else if(finalVector.x >= m_renderer->dim().x-SCROLL_BORDER) // Left
                dir.x = -1.f;

            if(finalVector.y <= SCROLL_BORDER)
                dir.y = -1.f;
            else if(finalVector.y >= m_renderer->dim().y-SCROLL_BORDER)
                dir.y = 1.f;

            if(notZeroVec(dir))
                m_camera->move(glm::vec3(glm::normalize(dir).y*DELTATIME, 0.f, glm::normalize(dir).x*DELTATIME));
        }
    }
    m_processor->updateStates();
    m_gamescene->updateScene();
}

void Core::removeObj(GenericObject3d *tobj)
{
    m_gamescene->remove(tobj);
}

void Core::connect(unistring ip, uint16_t port)
{

}

void Core::onClientJoin(unistring addr)
{
    if(m_netmgr->isClient())
    {
        m_audiomgr->playSound("onJoin", m_audiomgr->zero());
    }
}

void Core::onServerJoin(unistring addr)
{
    Logger::log("Core", "IP "+addr+" connected");
}

void Core::onDisconnect(unistring addr)
{
    Logger::log("Core", "IP "+addr+" disconnected");
}

int Core::elapsed()
{
    return m_renderer->stime();
}

void Core::register_lua_hooks()
{
    lua_register(Core::lua_shared, "Engine_Log", Hooker::log_hook);
    lua_register(Core::lua_shared, "Engine_Quit", Hooker::quit_hook);
    lua_register(Core::lua_shared, "Engine_Elapsed", Hooker::stime_hook);

    lua_register(Core::lua_shared, "UI_AddButton", Hooker::ui_addbutton_hook);
}

void Core::Lcall(unistring func)
{
    if(luaL_loadfile(Core::lua_shared, Core::curMap.c_str()))
        return;
    lua_getglobal(Core::lua_shared, func.c_str());
    lua_call(Core::lua_shared, 0, 0);
}

void Core::netUpdate()
{

}

GLuint Core::getCubemap(int id)
{
  return m_tloader->cubemap(id);
}

glm::vec2 &Core::mousePos()
{
    return m_processor->mousePos();
}

void Core::LaddUIbutton(unistring texture, unistring uiposn, unistring uidimn, unistring binding)
{
    glm::vec2 pos, dim;
    vector<unistring> dualparts;
    boost::split(dualparts, uiposn, is_any_of("x"), token_compress_off);
    if(dualparts.size() > 1)
    {
        pos.x = atoi(dualparts.at(0).c_str());
        pos.y = atoi(dualparts.at(1).c_str());
    }
    boost::split(dualparts, uidimn, is_any_of("x"), token_compress_off);
    if(dualparts.size() > 1)
    {
        dim.x = atoi(dualparts.at(0).c_str());
        dim.y = atoi(dualparts.at(1).c_str());
    }
    dualparts.clear();

    Logger::log("Core", m_uimgr->addElement(new UIButton(pos, dim, texture, binding)));
}

GameEvents *Core::events()
{
    return m_processor;
}
