#include <shader.h>

void Shader::loadShader(unistring fvname, unistring ffname, unistring fgname)
{
    if(ID != 0)
        return;
    GLuint vsh=0, fsh=0, gsh=0;

    unistring tvs, tfs, tgs; // TemporaryString (Vertex, Fragment, Geometry)

    GLint Result = GL_FALSE;
    int InfoLogLength;

    tvs.clear();
    ifstream vshader(fvname);
    getline(vshader, tvs, (char)vshader.eof());
    vshader.close();

    tfs.clear();
    ifstream fshader(ffname);
    getline(fshader, tfs, (char)fshader.eof());
    fshader.close();

    if(fgname != "none")
    {
      tgs.clear();
      ifstream gshader(fgname);
      getline(gshader, tgs, (char)gshader.eof());
      gshader.close();

      gsh = glCreateShader(GL_GEOMETRY_SHADER);

      // Geometry shader compilation
      printf("Compiling shader: %s\n", fgname.c_str());
      char const * gsh_src = tgs.c_str();
      glShaderSource(gsh, 1, &gsh_src, NULL);
      glCompileShader(gsh);

      // Geometry shader checking
      glGetShaderiv(vsh, GL_COMPILE_STATUS, &Result);
      glGetShaderiv(vsh, GL_INFO_LOG_LENGTH, &InfoLogLength);
      if ( InfoLogLength > 0 ){
          std::vector<char> GeometryShaderErrorMessage(InfoLogLength+1);
          glGetShaderInfoLog(gsh, InfoLogLength, NULL, &GeometryShaderErrorMessage[0]);
          fprintf(stdout, "%s\n", &GeometryShaderErrorMessage[0]);
      }
    }

    vsh = glCreateShader(GL_VERTEX_SHADER);
    fsh = glCreateShader(GL_FRAGMENT_SHADER);

    // Vertex shader compiling
    printf("Compiling shader: %s\n", fvname.c_str());
    char const * vsh_src = tvs.c_str();
    glShaderSource(vsh, 1, &vsh_src, NULL);
    glCompileShader(vsh);


    // Vertex shader checking
    glGetShaderiv(vsh, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(vsh, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(vsh, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);
    }


    // Fragment shader compiling
    printf("Compiling shader: %s\n", ffname.c_str());
    char const * fsh_src = tfs.c_str();
    glShaderSource(fsh, 1, &fsh_src, NULL);
    glCompileShader(fsh);


    // Fragment shader checking
    glGetShaderiv(fsh, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(fsh, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(fsh, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);
    }


    // Создаем шейдерную программу и привязываем шейдеры к ней
    fprintf(stdout, "Connecting shaders...\n");
    ID = glCreateProgram();
    glAttachShader(ID, vsh);
    glAttachShader(ID, fsh);
    if(gsh != 0)
      glAttachShader(ID, gsh);
    glLinkProgram(ID);


    // Проверяем шейдерную программу
    glGetProgramiv(ID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
    }

    fflush(stdout);
    //
    glDeleteShader(vsh);
    glDeleteShader(fsh);
    if(gsh != 0)
      glDeleteShader(gsh);
}

Shader::Shader(unistring vs, unistring fs, unistring gs) :
    ID(0)
{
  loadShader(vs, fs, gs);
}

void Shader::use()
{
    if(ID == 0)
        return;
    glUseProgram(ID);
}

void Shader::setMat4(unistring id, glm::mat4 &matrix)
{
    if(ID == 0 || id.empty())
        return;
    glUniformMatrix4fv(glGetUniformLocation(ID, id.c_str()), 1, GL_FALSE, &matrix[0][0]);
}

void Shader::setInt(unistring id, int value)
{
    glUniform1i(glGetUniformLocation(ID, id.c_str()), value);
}

void Shader::setFloat(unistring id, float value)
{
    glUniform1f(glGetUniformLocation(ID, id.c_str()), value);
}

void Shader::setVec3(unistring id, glm::vec3 &vec)
{
  glUniform3fv(glGetUniformLocation(ID, id.c_str()), 1, &vec[0]);
}

void Shader::setVec2(unistring id, glm::vec2 &vec)
{
  glUniform2fv(glGetUniformLocation(ID, id.c_str()), 1, &vec[0]);
}
