#include <hooks.h>
#include <core.h>

int Hooker::log_hook(lua_State* Ls)
{
    const char *msg = lua_tostring(Ls, 1);

    Logger::log("Lua", msg);

    return 0; // We return nothing, just printing to console.
}

int Hooker::quit_hook(lua_State* Ls)
{
    Core::c_shared->quit();
    return 0;
}

int Hooker::stime_hook(lua_State *Ls)
{
    lua_pushnumber(Ls, Core::c_shared->elapsed());

    return 1; // return n-times we pushed to stack, we did it once here.
}

int Hooker::ui_addbutton_hook(lua_State* Ls)
{
    unistring texn  = lua_tostring(Ls, 1);
    unistring posn  = lua_tostring(Ls, 2);
    unistring dimn  = lua_tostring(Ls, 3);
    unistring bindn = lua_tostring(Ls, 4);

    Core::c_shared->LaddUIbutton(texn, posn, dimn, bindn);

    return 0;
}
