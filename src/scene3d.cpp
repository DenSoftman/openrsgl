#include <scene3d.h>

Scene3d::Scene3d()
{

}

Scene3d::~Scene3d()
{
    for(auto &fo : m_objects)
    {
        if(obj(fo.first))
           delete obj(fo.first);
    }
}

const map<unistring, GenericObject3d *> &Scene3d::objs()
{
  return m_objects;
}

GenericObject3d *Scene3d::obj(unistring alias)
{
    if(m_objects.find(alias) != m_objects.end())
        return m_objects[alias];
    return NULL;
}

const map<unistring, ParticleSystem *> &Scene3d::particleSys()
{
  return m_pSystems;
}

ParticleSystem *Scene3d::psys(unistring alias)
{
    if(m_pSystems.find(alias) != m_pSystems.end())
        return m_pSystems[alias];
    return NULL;
}

void Scene3d::setParticle(unistring alias, int pc, int md, float mxs, float ps)
{
    if(psys(alias))
        psys(alias)->setParticle(pc, md, mxs, ps);
}

void Scene3d::moveObj(unistring alias, glm::vec3 mvmnt)
{
    if(obj(alias))
        obj(alias)->move(mvmnt);
}

void Scene3d::rotateObj(unistring alias, glm::vec3 axis, float angle)
{
    if(obj(alias))
        obj(alias)->rotate(axis, angle);
}

void Scene3d::setObjRotation(unistring alias, glm::vec3 newRotation)
{
    if(obj(alias))
        obj(alias)->setRotation(newRotation);
}

void Scene3d::resizeObj(unistring alias, float factor)
{
    if(obj(alias))
        obj(alias)->scale(factor);
}

const glm::vec3 &Scene3d::getObjPos(unistring alias)
{
    if(obj(alias))
        return obj(alias)->getPos();
    else
        return glm::vec3(0.f);
}

void Scene3d::select(unistring i)
{
    if(obj(i))
        m_objects[i]->select(true);
}

void Scene3d::deselect(unistring i)
{
    if(obj(i))
        m_objects[i]->select(false);
}
void Scene3d::addObject(GLuint addr, uint sz, GLuint tid, unistring alias,
                        bool spawn, glm::vec3 initPos, glm::vec3 initRot,
                        glm::vec3 accel, glm::vec3 bounds, int bt,
                        unistring logicType, float scale)
{
    Object3d* tmp = new Object3d;

    tmp->setModel(addr, sz);
    tmp->setTex(tid);
    tmp->setPos(initPos);
    tmp->setRotation(initRot);
    tmp->setAccel(accel);
    tmp->setAlias(alias);
    tmp->setBounds(bounds, bt);
    tmp->setLogicType(logicType);
    tmp->scale(scale);

    if(spawn)
    {
        m_objects[alias] = tmp;
    }
    else
        toAdd.push_back(tmp);
}

void Scene3d::addPSys(unistring alias, int mode)
{
  m_pSystems[alias] = new ParticleSystem;
}

void Scene3d::remove(GenericObject3d *tobj)
{
    if(!tobj)
        return;
    if(find(toDelete.begin(), toDelete.end(), tobj) == toDelete.end()) // No duplicates
        toDelete.push_back(tobj);
}

void Scene3d::attachObject(GenericObject3d *gobj)
{
    if(!gobj)
        return;
    if(find(toAdd.begin(), toAdd.end(), gobj) == toAdd.end()) // No duplicates
        toAdd.push_back(gobj);
}

void Scene3d::updateScene()
{
    for(GenericObject3d* al : toDelete)
    {
        del(al);
    }
    toDelete.clear();

    for(GenericObject3d* ao : toAdd)
    {
        del(m_objects[ao->getAlias()]);
        m_objects[ao->getAlias()] = ao;
    }
    toAdd.clear();
}

void Scene3d::del(GenericObject3d *target)
{
    if(!target) // Not null
        return;

    auto it = m_objects.begin();
    while(it != m_objects.end())
    {
        if(it->second == target)
        {
            m_objects.erase(it);
            break;
        }
        ++it;
    }
    delete target; // !!! DON'T FORGET TO FREE MEMORY !!!
}
