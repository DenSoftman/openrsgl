#include <maploader.h>
#include <core.h>

MapLoader::MapLoader()
{
    LIBXML_TEST_VERSION     // Check integrity
    xmlInitParser();        // Init libxml
}

void MapLoader::loadMap(unistring mf, unistring ma, map<unistring, unistring> *mdls, map<unistring, unistring> *texs, map<unistring, unistring> *sounds, map<unistring, unistring> *music)
{
    if(luaL_loadfile(Core::lua_shared, mf.c_str()))
    {
        Logger::err("MapLoader", unistring("Failed to open ")+mf);
        return;
    }
    lua_pcall(Core::lua_shared, 0, 0, 0);

    lua_getglobal(Core::lua_shared, "Minfo");
    unistring mname = LuaString(Core::lua_shared, "Mname");
    unistring mdesc = LuaString(Core::lua_shared, "Mdesc");
    //
    Map3d* mapPtr;

    curDoc = xmlParseFile((unistring(MAPS_ROOT)+mdesc).c_str());
    if(curDoc == NULL)
    {
        Logger::err("MapLoader", "No such map file: "+mf);
        return;
    }
    //
    curNode = xmlDocGetRootElement(curDoc);
    if(curNode == NULL)
    {
        xmlFreeDoc(curDoc);
        Logger::err("MapLoader", "Failed to retrieve node properties");
        return;
    }

    mapPtr = new Map3d; // Allocate memory for map3d
    ESAsL_Parser *sparser = new ESAsL_Parser;

    if(sparser == NULL || mapPtr == NULL)
    {
        Logger::err("MapLoader", "Failed to init scriptparser");
        xmlFreeDoc(curDoc);
        return;
    }
    mapPtr->setName(ma);
    mapPtr->attachParser(sparser);
    unistring nodeName, attrName;
    for(xmlNodePtr node=curNode->children; node; node = node->next)
    {
        nodeName = xc2str(node->name);
        if(nodeName == "preload")
        {
            for(xmlNodePtr childNode=node->children; childNode; childNode = childNode->next)
            {
                if(xc2str(childNode->name) == "comment")
                    continue;
                if(xc2str(childNode->name) == "text")
                    continue;

                if(xc2str(childNode->name) == "model")
                {
                    if(mdls == NULL)
                        continue;
                    for(xmlAttrPtr nodeAttr=childNode->properties; nodeAttr; nodeAttr=nodeAttr->next)
                    {
                        attrName = xc2str(nodeAttr->name);
                        if(attrName == "file")
                        {
                            unistring mn = nodeStrData(childNode);
                            boost::trim(mn);
                            mdls->insert( std::pair<unistring, unistring> (mn, attrData(childNode, nodeAttr)));
                        }
                    }
                }
                else if(xc2str(childNode->name) == "texture")
                {
                    if(texs == NULL)
                        continue;
                    for(xmlAttrPtr nodeAttr=childNode->properties; nodeAttr; nodeAttr=nodeAttr->next)
                    {
                        attrName = xc2str(nodeAttr->name);
                        if(attrName == "file")
                        {
                            unistring tn = nodeStrData(childNode);
                            boost::trim(tn);
                            texs->insert( std::pair<unistring, unistring> (tn, attrData(childNode, nodeAttr)));
                        }
                    }
                }
                else if(xc2str(childNode->name) == "sound")
                {
                    if(sounds == NULL)
                        continue;
                    for(xmlAttrPtr nodeAttr=childNode->properties; nodeAttr; nodeAttr=nodeAttr->next)
                    {
                        attrName = xc2str(nodeAttr->name);
                        if(attrName == "file")
                        {
                            unistring sn = nodeStrData(childNode);
                            boost::trim(sn);
                            sounds->insert( std::pair<unistring, unistring> (sn, attrData(childNode, nodeAttr)));
                        }
                    }
                }
                else if(xc2str(childNode->name) == "music")
                {
                    if(music == NULL)
                        continue;
                    for(xmlAttrPtr nodeAttr=childNode->properties; nodeAttr; nodeAttr=nodeAttr->next)
                    {
                        attrName = xc2str(nodeAttr->name);
                        if(attrName == "file")
                        {
                            unistring musn = nodeStrData(childNode);
                            boost::trim(musn);
                            music->insert( std::pair<unistring, unistring> (musn, xc2str(xmlGetNoNsProp(childNode, (xmlChar*)"file"))));
                        }
                    }
                }
            }
            break;
        }
    }
    xmlFreeDoc(curDoc);
//
    lua_getglobal(Core::lua_shared, "Minit");
    lua_call(Core::lua_shared, 0, 0);
//
    a_maps[ma] = mapPtr;
}

Map3d *MapLoader::mapPtr(unistring ma)
{
    return a_maps[ma];
}
