#include <orsgl_worker.h>

ORSGL_Worker::ORSGL_Worker()
{
    ORSGL_Worker(nullptr);
}

ORSGL_Worker::ORSGL_Worker(CharacterInfo *info)
{
    if(info == nullptr)
    {
        m_cinfo = new CharacterInfo;
        m_state = FREE_STATE;
    }
    else
    {
        m_cinfo = info;
    }
}
