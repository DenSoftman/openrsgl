#include <logger.h>

void Logger::log(unistring module, unistring msg)
{
    std::cout << "\n[L] [ " << module << " ] " << msg << std::endl;
}

void Logger::err(unistring module, unistring msg)
{
    std::cout << "\n<E> [ " << module << " ] " << msg << std::endl;
}

void Logger::warn(unistring module, unistring msg)
{
    std::cout << "\n[L] [ " << module << " ] { " << msg << " }" << std::endl;
}

void Logger::info(unistring module, unistring msg)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,
                             module.c_str(),
                             msg.c_str(),
                             nullptr);
}
