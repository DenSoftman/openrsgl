#include <object3d.h>
#include <core.h>

Object3d::Object3d()
{
    Object3d("none", "none");
}

Object3d::Object3d(unistring al, unistring owner) :
    m_owner(owner),
    m_scale(1.f),
    pos(glm::vec3(0,0,0)),
    rot(glm::vec3(0,0,0)),
    ModelMatrix(glm::mat4(1.f)),
    m_vao(0),
    m_selected(false),
    a_pos{0.0, 0.0, 0.0},
    curAnim("none"), curFrame(0), maxFrame(0),
    m_alias(al),
    logicType("")
{
    alGenSources(1, &a_src);
    alSourcef(a_src, AL_PITCH, 1.f);
    alSourcef(a_src, AL_GAIN, 1.f);
    alSourcefv(a_src, AL_POSITION, a_pos);
    alSourcei(a_src, AL_LOOPING, AL_FALSE);
    alSourcei(a_src, AL_SOURCE_RELATIVE, AL_FALSE);

    GenericObject3d::objCount++;
}

Object3d::~Object3d()
{
    GenericObject3d::objCount--;
}

void Object3d::draw(Shader *shader)
{
    ModelMatrix = glm::scale(glm::mat4(), glm::vec3(m_scale, m_scale, m_scale)) * glm::translate(glm::mat4(), pos) * (glm::rotate(glm::mat4(), rot.x, glm::vec3(1,0,0))*glm::rotate(glm::mat4(), rot.y, glm::vec3(0,1,0))*glm::rotate(glm::mat4(), rot.z, glm::vec3(0,0,1)) );
    shader->setMat4("Model", ModelMatrix);

    glBindVertexArray(m_vao);
        glActiveTexture(GL_TEXTURE0);           // 0 here  |
        glBindTexture(GL_TEXTURE_2D, m_texid);  //         | for this
        shader->setInt("mainTexture", 0);       // 0 there |
        glDrawArrays(GL_TRIANGLES, 0, m_size);
        glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);

    if(curAnim != "none" && curAnim != "")
    {
      if(curFrame == maxFrame)
        curFrame = 0;
      setVAO(Core::c_shared->getAnimFrame(curAnim, curFrame));
      curFrame++;
    }

    if(notZeroVec(m_accel))
        pos += m_accel * DELTATIME;

    if(notZeroVec(m_bounds))
    {
        if(m_bType == 1) // B_GREATER
        {
            if(m_bounds.x != 0.f)
            {
                if(pos.x >= m_bounds.x)
                {
                    Core::c_shared->removeObj(this);
                }
            }
            if(m_bounds.y != 0.f)
            {
                if(pos.y >= m_bounds.y)
                {
                    Core::c_shared->removeObj(this);
                }
            }
            if(m_bounds.z != 0.f)
            {
                if(pos.z >= m_bounds.z)
                {
                    Core::c_shared->removeObj(this);
                }
            }
        }
        else if(m_bType == -1)// B_LESS
        {
            if(m_bounds.x != 0)
            {
                if(pos.x <= m_bounds.x)
                {
                    Core::c_shared->removeObj(this);
                }
            }
            if(m_bounds.y != 0)
            {
                if(pos.y <= m_bounds.y)
                {
                    Core::c_shared->removeObj(this);
                }
            }
            if(m_bounds.z != 0)
            {
                if(pos.z <= m_bounds.z)
                {
                    Core::c_shared->removeObj(this);
                }
            }
        }
    }
}

void Object3d::setTex(GLuint texid)
{
  m_texid = texid;
}

void Object3d::setVAO(GLuint vao)
{
  m_vao = vao;
}

void Object3d::setAnim(unistring anim, uint_fast16_t last)
{
  curAnim = anim;
  maxFrame = last;
}

void Object3d::setAccel(glm::vec3 accel)
{
    m_accel = accel;
}

void Object3d::setLogicType(unistring newtype)
{
    logicType = newtype;
}

const unistring &Object3d::getLogicType()
{
    return logicType;
}

void Object3d::setState(uint_fast8_t state)
{
    m_state = state;
}

uint_fast8_t Object3d::getState()
{
    return m_state;
}

void Object3d::setOwner(unistring owner)
{
    m_owner = owner;
}

const unistring &Object3d::getOwner()
{
    return m_owner;
}

void Object3d::setAlias(unistring other)
{
    m_alias = other;
}

const unistring &Object3d::getAlias()
{
    return m_alias;
}

void Object3d::move(glm::vec3 mv)
{
    pos += mv;

    a_pos[0] = pos.x;
    a_pos[1] = pos.y;
    a_pos[2] = pos.z;
    //ModelMatrix = glm::translate(ModelMatrix, pos);
}

void Object3d::rotate(glm::vec3 axis, float angle)
{
    rot += glm::normalize(axis) * glm::radians(angle);
    //ModelMatrix = glm::rotate(ModelMatrix, rot.x, glm::vec3(1,0,0))*glm::rotate(ModelMatrix, rot.y, glm::vec3(0,1,0))*glm::rotate(ModelMatrix, rot.z, glm::vec3(0,0,1));
}

void Object3d::setRotation(glm::vec3 newrot)
{
    rot = glm::vec3(glm::radians(newrot.x), glm::radians(newrot.y), glm::radians(newrot.z));
}

void Object3d::scale(float factor)
{
    m_scale = factor;
}

void Object3d::setPos(glm::vec3 newpos)
{
    pos = newpos;
}

const glm::vec3 &Object3d::getPos()
{
    return pos;
}

void Object3d::setBounds(glm::vec3 bounds, int bt)
{
    m_bounds = bounds;
    m_bType = bt;
}

void Object3d::select(bool yes)
{
    m_selected = yes;
}

bool Object3d::isSelected()
{
    return m_selected;
}

GLuint Object3d::vertexCount()
{
    return m_size;
}

void Object3d::setModel(GLuint addr, uint sz)
{
    m_vao = addr;
    m_size = sz;
}

ALuint Object3d::audiosource()
{
    return a_src;
}
