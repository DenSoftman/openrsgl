#include <loader3d.h>

Loader3D::Loader3D()
{

}

Loader3D::~Loader3D()
{

}

void Loader3D::LoadModelOBJ(unistring fname, vector<GLfloat> *vertexData)
{
    ifstream fin(fname);
    if(!fin.is_open())
    {
        Logger::err("Loader3D", "No such OBJ file: "+fname);
        return;
    }

    unistring ts;
    unistrlist parts;
    vector <glm::vec3> coords;
    vector <glm::vec2> texcoords;
    vector <glm::vec3> normals;
    unistrlist trio;

    vertexData->clear();
    while (getline(fin, ts))
    {
        parts.clear();
        if (ts[0] == 'v') // Vertex coords
        {
            if(ts[1] == 't') // Texture coords
            {
                split(parts, ts, is_any_of(" "));
                if(parts.size() != 3)
                    break;
                texcoords.push_back( glm::vec2( atof(parts.at(1).c_str()), atof(parts.at(2).c_str()) ) );
            }
            else if(ts[1] == 'n') // Normal coords
            {
                split(parts, ts, is_any_of(" "));
                if(parts.size() < 4)
                    break;
                normals.push_back( glm::vec3(atof(parts.at(1).c_str()), atof(parts.at(2).c_str()), atof(parts.at(3).c_str()) ) );
            }
            else
            {
                split(parts, ts, is_any_of(" "));
                if(parts.size() < 4)
                    break;
                coords.push_back( glm::vec3(atof(parts.at(1).c_str()), atof(parts.at(2).c_str()), atof(parts.at(3).c_str()) ) );
            }
        }
        else if (ts[0] == 'f')
        {
            split(parts, ts, is_any_of(" "));
            if (parts.size() != 4)
                break;

            for (int i = 1; i < 4; i++)
            {
                trio.clear();
                split(trio, parts.at(i), is_any_of("/"));

                if(trio.size() < 3)
                    break;

                vertexData->push_back( coords.at( atol(trio.at(0).c_str())-1).x );
                vertexData->push_back( coords.at( atol(trio.at(0).c_str())-1).y );
                vertexData->push_back( coords.at( atol(trio.at(0).c_str())-1).z );

                if(texcoords.size() > 0)
                {
                    vertexData->push_back( texcoords.at( atol(trio.at(1).c_str())-1).x );
                    vertexData->push_back( texcoords.at( atol(trio.at(1).c_str())-1).y );
                }
                else
                {
                    vertexData->push_back( 0.f );
                    vertexData->push_back( 0.f );
                }

                vertexData->push_back( normals.at( atol(trio.at(2).c_str())-1).x );
                vertexData->push_back( normals.at( atol(trio.at(2).c_str())-1).y );
                vertexData->push_back( normals.at( atol(trio.at(2).c_str())-1).z );
            }
        }
    }
    fin.close();
}

void Loader3D::LoadModelDAE(unistring fname, vector<GLfloat> *vertexData)
{
    xmlDocPtr curDoc = xmlParseFile(fname.c_str());
    if(curDoc == NULL)
        return;

    xmlNodePtr curNode = xmlDocGetRootElement(curDoc);
    xmlNodePtr childNode=NULL, paramNode=NULL, elementNode=NULL;

    vector <glm::vec3> coords;
    vector <glm::vec2> texcoords;
    vector <glm::vec3> normals;
    vertexData->clear();

    for(xmlNodePtr node=curNode->children; node; node=node->next)
    {
        unistring mainNodeName = xc2str(node->name);
        if(mainNodeName == "asset")
        {
            for(childNode = node->children; childNode; childNode = childNode->next)
            {
                if(xc2str(childNode->name) == "up_axis")
                {

                }
            }
        }
        else if(mainNodeName == "library_effects")
        {

        }
        else if(mainNodeName == "library_materials")
        {

        }
        else if(mainNodeName == "library_geometries")
        {
            for(childNode = node->children; childNode; childNode = childNode->next) // Geometry array
            {
                for(paramNode = childNode->children; paramNode; paramNode = paramNode->next) // Mesh arrays
                {
                    for(xmlNodePtr meshNode = paramNode->children; meshNode; meshNode = meshNode->next) // Mesh param array
                    {
                        if(xc2str(meshNode->name) == "source")
                        {
                            unistring attrStr = attrData(meshNode, "id");
                            if(attrStr.find("position") != unistring::npos)
                            {
                                for(elementNode = meshNode->children; elementNode; elementNode = elementNode->next)
                                {
                                    if(xc2str(elementNode->name) == "float_array") // Vertex positions
                                    {
                                        if(attrData(elementNode, "count").length() > 0)
                                        {
                                            unistring tmp = nodeStrData(elementNode);
                                            unistrlist tmpArr;
                                            boost::split(tmpArr, tmp, is_any_of(" "), token_compress_on);
                                            for(int i=0; i < atoi(attrData(elementNode, "count").c_str()); i += 3) // 3 coords
                                            {
                                                coords.push_back(glm::vec3(atof(tmpArr.at(i+0).c_str()),
                                                                           atof(tmpArr.at(i+1).c_str()),
                                                                           atof(tmpArr.at(i+2).c_str())));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if(xc2str(meshNode->name) == "triangles")
                        {
                            for(elementNode = meshNode->children; elementNode; elementNode = elementNode->next)
                            {
                                if(xc2str(elementNode->name) == "p") // Faces
                                {
                                    vector<unistring> faces;
                                    unistring tmp = nodeStrData(elementNode);
                                    boost::split(faces, tmp, is_any_of(" "), token_compress_off);
                                    size_t fcount = atoi( attrData(meshNode, "count").c_str() );
                                    for(int i=0; i < fcount*6; i += 6) // 3 vertexes and 3 normals
                                    {
                                        glm::vec3 tmp = glm::vec3(atol(faces.at(i+0).c_str()), atol(faces.at(i+2).c_str()), atol(faces.at(i+4).c_str()));

                                        vertexData->push_back( coords.at( tmp.x ).x );
                                        vertexData->push_back( coords.at( tmp.y ).y );
                                        vertexData->push_back( coords.at( tmp.z ).z );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if(mainNodeName == "library_visual_scenes")
        {

        }
        else if(mainNodeName == "scene")
        {

        }
    }
}
