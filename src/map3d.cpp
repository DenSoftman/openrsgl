#include <map3d.h>

Map3d::Map3d()
{

}

void Map3d::addObjInfo(ObjectInfo *info)
{
    m_objOnMap.push_back(info);
}

void Map3d::attachParser(ESAsL_Parser *parser)
{
    m_scriptParser = parser;
}

void Map3d::attachScript(ESAsL_Script *other)
{
    m_scripts.push_back(other);
}

void Map3d::setName(unistring newalias)
{
    m_alias = newalias;
}

void Map3d::attachAudio(unistring sndn, bool looped, bool snd, bool mus)
{
    if(snd)
    {
        if(looped)
        {
            m_bgMusic.push_back(sndn);
            Logger::log("Map3d", "Sound added to queue: "+sndn);
        }
    }
    else if(mus)
    {

    }
}

vector<unistring> *Map3d::bgMusic()
{
    return &m_bgMusic;
}

vector<ObjectInfo *> *Map3d::objs()
{
    return &m_objOnMap;
}

ObjectInfo *Map3d::obj(int id)
{
    return objs()->at(id);
}

const unistring Map3d::getAlias()
{
    return m_alias;
}

void Map3d::setLight(glm::vec3 pos, int type)
{
    mainLightPosition = pos;
    mainLightType = type;
}

const glm::vec3 &Map3d::getLightPos()
{
    return mainLightPosition;
}

int Map3d::getLightType()
{
    return mainLightType;
}
