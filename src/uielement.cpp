#include <uielement.h>


void UIElement::scale(float factor)
{
    m_dim *= factor;
}

const unistring &UIElement::texn()
{
    return m_texn;
}

void UIElement::setTex(unistring tn)
{
  m_texn = tn;
}

glm::vec2 &UIElement::pos()
{
  return m_pos;
}

int UIElement::w()
{
  return m_dim.x;
}

int UIElement::h()
{
  return m_dim.y;
}

int UIElement::px()
{
  return m_pos.x;
}

int UIElement::py()
{
  return m_pos.y;
}

void UIElement::update(glm::vec2 &mousePos)
{

}
