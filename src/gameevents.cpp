#include <gameevents.h>

GameEvents::GameEvents()
{
    mouse_state = "none";
    oldmpos.x = 0;  oldmpos.y = 0;

    button_down[0] = false;
    button_down[1] = false;
    button_down[2] = false;

    button_clicked[0] = false;
    button_clicked[1] = false;
    button_clicked[2] = false;
}

glm::vec2 &GameEvents::mousePos()
{
    return m_mpos;
}

void GameEvents::mousePos(glm::vec2 newPos)
{
  m_mpos = newPos;
}

bool GameEvents::mouseRect(int mode, glm::vec2 pos, glm::vec2 dim)
{
  if(mode == INSIDE)
  {
      if(m_mpos.x <= pos.x + dim.x && m_mpos.x >= pos.x &&
         m_mpos.y <= pos.y + dim.y && m_mpos.y >= pos.y)
        return true;
      return false;
  }
  else if(mode == OUTSIDE)
  {
      return !(mouseRect(INSIDE, pos, dim));
  }
  return false;
}

bool GameEvents::isMouseDown(int mbtn)
{
    return button_down[mbtn-1]; // 1 - left, 2 - middle, 3 - right
}

bool GameEvents::isMouseUp(int mbtn)
{
    return !button_down[mbtn-1];
}

bool GameEvents::isMouseClicked(int mbtn)
{
    return button_clicked[mbtn-1];
}

bool GameEvents::keyDown(int scancode)
{
  return SDL_GetKeyboardState(nullptr)[scancode];
}

void GameEvents::updateStates()
{
  // Mouse update
  int x,y;
  SDL_GetMouseState(&x, &y);
  m_mpos = glm::vec2(x, y);

  if(isMouseClicked(SDL_BUTTON_LEFT))
      button_clicked[0] = false;
  if(isMouseClicked(SDL_BUTTON_MIDDLE))
      button_clicked[1] = false;
  if(isMouseClicked(SDL_BUTTON_RIGHT))
      button_clicked[2] = false;
  //
}
