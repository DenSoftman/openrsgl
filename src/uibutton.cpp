#include <uibutton.h>
#include <core.h>

UIButton::UIButton()
{

}

UIButton::UIButton(glm::vec2 pos, glm::vec2 dim, unistring texn)
{
    m_pos = pos;
    m_dim = dim;
    m_texn = texn;
    m_onActivated.clear();
}

UIButton::UIButton(glm::vec2 pos, glm::vec2 dim, unistring texn, unistring fbind)
{
    m_pos = pos;
    m_dim = dim;
    m_texn = texn;
    m_onActivated = fbind;
}

void UIButton::scale(float factor)
{
    m_dim *= factor;
}

void UIButton::update(glm::vec2 &mousePos)
{
    if(mousePos.x >= m_pos.x &&
       mousePos.x <= m_pos.x + m_dim.x &&
       mousePos.y >= m_pos.y &&
       mousePos.y <= m_pos.y + m_dim.y)
    {
        if(Core::c_shared->events()->isMouseDown(SDL_BUTTON_LEFT) && !isPressed)
        {
            isPressed = true;
            Core::c_shared->Lcall(m_onActivated);
        }

        if(!isHovered)
        {
            isHovered = true;
            scale(1.3f);
        }

    }
    else
    {
        if(isHovered)
        {
            isHovered = false;
            scale(1.f/1.3f);
        }
    }

    if(!Core::c_shared->events()->isMouseDown(SDL_BUTTON_LEFT) && isPressed)
    {
        isPressed = false;
    }
}

void UIButton::setTex(unistring tn)
{
  m_texn = tn;
}

glm::vec2 &UIButton::pos()
{
  return m_pos;
}

int UIButton::w()
{
  return m_dim.x;
}

int UIButton::h()
{
  return m_dim.y;
}

int UIButton::px()
{
  return m_pos.x;
}

int UIButton::py()
{
    return m_pos.y;
}

const unistring &UIButton::texn()
{
    return m_texn;
}
