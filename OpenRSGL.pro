TARGET = OpenRSGL

CONFIG += c++17 console
CONFIG -= qt

INCLUDEPATH =  /usr/include/          \
               /usr/include/libxml2/  \
               include/

SOURCES += src/* main.cpp \
    src/uibutton.cpp

HEADERS += include/* \
    include/uibutton.h

DISTFILES += data/shaders/*
