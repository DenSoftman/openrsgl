--[[
    Simple config file for OpenRSGL engine.

    Learn more on https://gitlab.com/DenSoftman/OpenRSGL
]]--

appName = "OpenRSGL"
version = "0.1b"

engineConf =
{
    title       = appName.." "..version,
    
    width       = 640,     -- Width of window
    height      = 480,      -- Height of window
    fullscreen  = 0,        -- 0 or 1
    
    firstlevel  = "main.lm",
    
    start_script = "begin.lua",
    
    scene_root  ="data/maps",

    models_root ="data/models",
    textures_root="data/img",

    scripts_root="data/scripts",

    sounds_root="data/audio/sounds",
    music_root="data/audio/music"
}

function readConf()
    local fio = io.open("engine.conf", "r")
    if fio then
        fio:close()
        for line in io.lines("engine.conf") do
            local parts = {}
            for w in line:gmatch("%w+") do
                table.insert(parts, w)
            end
            
            -- ========================================
            if parts[1] == "width" then
                engineConf.width = tonumber(parts[2])
            end
            
            if parts[1] == "height" then
                engineConf.height = tonumber(parts[2])
            end
            
            if parts[1] == "fullscreen" then
                engineConf.fullscreen = tonumber(parts[2])
            end
            -- ========================================
        end
    end
end

function Engine_OnGameStart() -- This function executes one time at game start
    readConf()
end