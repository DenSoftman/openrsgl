#version 320 es
uniform sampler2D mainTexture;

in highp vec3 normalPos;
in highp vec3 lightCoord;
in highp vec3 lightVector;
in lowp vec2 texPos;

out lowp vec4 fragColor;

uniform lowp int selected;
uniform lowp  vec3 lightColor;
uniform lowp float lightPower;

//highp float PHI = 1.61803398874989484820459*00000.1; // Golden Ratio
//highp float SQ2 = 1.41421356237309504880169*10000.0; // Square Root of Two


//highp float gold_noise(in highp vec2 coordinate, in highp float seed){
//    return fract(tan(distance(coordinate*(seed+PHI), vec2(PHI, PI)))*SQ2);
//}

uniform lowp int light;
uniform lowp int lightType; // LIGHT_SUN=1, LIGHT_POINT=0

void main(){
  if(light == 1)
  {
    highp vec4 finalLightColor=vec4(0.0);
    highp vec3 diffuseColor=vec3(0.0), specularColor=vec3(0.0), reflectDirection=vec3(0.0);
    highp float diffuseLightAmount, specularLightAmount;

    if(lightType == 0)
    {
      highp float dotp        = dot(normalize(normalPos), normalize(lightVector));
      diffuseLightAmount = max(dotp, 0.1f); // Two parameters, first is what amount of light if in line of sight, second is if object is out of light vision, may be > 0.f

      diffuseColor = diffuseLightAmount * lightColor;
      finalLightColor = vec4(diffuseColor, 1.0);
    }
    else if(lightType == 1)
    {
      highp vec3 lightDirection = -lightCoord.xyz;
      reflectDirection = reflect(lightDirection, normalPos);
      diffuseLightAmount = max(dot(normalPos, lightDirection), 0.1);

      diffuseColor = diffuseLightAmount * lightColor;
      finalLightColor = vec4(diffuseColor, 1.0);
    }

    fragColor = texture2D(mainTexture, texPos) * finalLightColor;
  }
  else
  {
    fragColor = texture2D(mainTexture, texPos);
  }
} 
