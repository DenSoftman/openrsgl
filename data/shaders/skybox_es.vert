#version 320 es
layout (location = 0) in highp vec3 vertPos;

out highp vec3 TexCoords;

uniform highp mat4 View;

void main()
{
    TexCoords = vertPos;
    vec4 pos = View * vec4(vertPos, 1.0);
    gl_Position = pos.xyww;
}
