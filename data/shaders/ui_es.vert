#version 320 es
layout(location = 0) in highp vec3 vertCoord;
layout(location = 1) in highp vec2 texCoord;

out highp vec2 texCoordOut;

uniform highp mat4 uimatx;

void main()
{
    gl_Position = uimatx * vec4(vertCoord, 1.0);
    texCoordOut = texCoord;
}
