#version 320 es
uniform sampler2D renderTexture;

in highp vec2 texPos; //this is the texture coord

highp float PI  = 3.141592; // PI

out highp vec4 fragColor;

uniform highp int stime;

void main(void)
{
    lowp float brightness = 1.2f;

    highp vec2 texCoord = texPos;
    //texCoord.x += sin(texCoord.y * 8.f * PI + (stime / 1000.f * 2.f * PI))/100.f;
    fragColor = texture2D(renderTexture, texCoord) * brightness;
}
