#version 320 es
uniform sampler2D texImage;

in highp vec2 texPos;
out lowp vec4 fragColor;

//highp float PHI = 1.61803398874989484820459*00000.1; // Golden Ratio
//highp float SQ2 = 1.41421356237309504880169*10000.0; // Square Root of Two


//highp float gold_noise(in highp vec2 coordinate, in highp float seed){
//    return fract(tan(distance(coordinate*(seed+PHI), vec2(PHI, PI)))*SQ2);
//}

uniform lowp float intensity;

void main()
{
  lowp vec4 col = texture2D(texImage, texPos);

  col = vec4(0.f, 1.f, 0.f, 1.f);

  if(intensity == 0.f)
    fragColor = vec4(col.xyz, 1.f);
  else
    fragColor = vec4(col.xyz*intensity, 1.f);
} 
 
