#version 320 es
uniform samplerCube skybox;

in highp vec3 TexCoords;

out highp vec4 finalColor;

void main()
{
    finalColor = textureCube(skybox, TexCoords);
}
