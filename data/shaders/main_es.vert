#version 320 es
layout(location = 0) in highp vec3 vertCoord;
layout(location = 1) in highp vec2 texCoord;
layout(location = 2) in highp vec3 normalCoord;

out highp vec2 texPos;
out highp vec3 fragPos;

out highp vec3 normalPos;
out highp vec3 lightVector;
out highp vec3 lightCoord;

uniform highp vec3 lightPos;
uniform highp mat4 View, Model;

void main()
{
  highp vec4 worldPos   = Model * vec4(vertCoord, 1.0);

  texPos  = texCoord;
  normalPos    = vec4(Model * vec4(normalCoord, 0.0)).xyz;
  lightCoord   = lightPos;
  lightVector  = lightPos - worldPos.xyz;

  gl_Position = View * worldPos;
}
