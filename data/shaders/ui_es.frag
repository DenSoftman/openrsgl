#version 320 es
uniform sampler2D uiTex;
in highp vec2 texCoordOut;

out highp vec4 fragColor;

void main(void)
{
  fragColor = texture2D(uiTex, texCoordOut);
}
