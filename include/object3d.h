#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <audiomanager.h>
#include <config.h>
#include <depends.h>
#include <genericobject3d.h>
#include <shader.h>
#include <texloader.h>

class Core;

class Object3d : public GenericObject3d
{
public:
    Object3d();
    Object3d(unistring al, unistring owner);
    ~Object3d();
    void draw(Shader* shader);

    void setTex(GLuint texid);
    void setVAO(GLuint vao);

    void setAlias(unistring other);
    const unistring& getAlias();

    void move(glm::vec3 mv);
    void rotate(glm::vec3 axis, float angle);
    void setRotation(glm::vec3 newrot);
    void scale(float factor);
    void setPos(glm::vec3 newpos);
    const glm::vec3& getPos();
    void setBounds(glm::vec3 bounds, int bt);

    void setModel(GLuint addr, uint sz);

    void select(bool yes);
    bool isSelected();
    GLuint vertexCount();
    ALuint audiosource();

    void setAnim(unistring anim, uint_fast16_t last = 60);
    void setAccel(glm::vec3 accel);

    void setLogicType(unistring newtype);
    const unistring& getLogicType();

    void setState(uint_fast8_t state);
    uint_fast8_t getState();

    void setOwner(unistring owner);
    const unistring& getOwner();

protected:
    unistring m_owner;
    unistring m_alias;
    bool m_selected;
    uint_fast8_t m_state;

    GLuint m_vao, m_size, m_texid; // texid for opengl, size & vao for glDrawArrays
    glm::vec3 pos, rot;
    float m_scale;
    glm::mat4 ModelMatrix;
    // Audio
    ALfloat a_pos[3]; // Audio pos
    ALuint a_src;     // Audio source
private:
    unistring logicType; // player, enemy, bullet
    glm::vec3 m_accel;
    glm::vec3 m_bounds; // Bounds out of whose object will be deleted (for bullets?)
    int m_bType;
    // Animation
    // uint_fast8_t framerate; // 60fps, usually; We'll use TARGET_FPS defined value
    unistring curAnim;
    uint_fast16_t curFrame; // Must!!! be < 65535, it's ok, i think
    uint_fast16_t maxFrame; // Last frame ID, for 120-framed animation is 120, last frame is 119
};

#endif // OBJECT3D_H
