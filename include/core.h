#ifndef CORE_H
#define CORE_H

#include <audiomanager.h>
#include <camera.h>
#include <depends.h>
#include <esasl_parser.h>
#include <gameevents.h>
#include <logger.h>
#include <map3d.h>
#include <maploader.h>
#include <mdlloader.h>
#include <networkmanager.h>
#include <orsgl_worker.h>
#include <renderer.h>
#include <scene3d.h>
#include <shader.h>
#include <texloader.h>
#include <uimanager.h>

#include <hooks.h>

class Core
{
public:
    Core();
    ~Core();
    void init(bool server);
    void cleanup();
    int exec();
    void quit();

    void draw_objs3D();

    void preloadData();
    void clearPreloadLists();

    void loadMaps();
    void loadMapToScene(Map3d* other);

    GLuint getAnimFrame(unistring anim, uint_fast16_t frame);
    GLuint textureID(unistring texn);

    void processEvents();

    void removeObj(GenericObject3d* tobj);

    void connect(unistring ip, uint16_t port);
    //
    void onClientJoin(unistring addr);
    void onServerJoin(unistring addr);
    void onDisconnect(unistring addr);

    int elapsed();

    void register_lua_hooks();
    void Lcall(unistring func);

    void netUpdate();
    //
    GLuint getCubemap(int id);
    glm::vec2 &mousePos();
    //
    void LaddUIbutton(unistring texture, unistring uiposn, unistring uidimn, unistring binding);

    GameEvents* events();

    static Core* c_shared; // Engine-only var
    static lua_State* lua_shared; // Engine-only var
    static unistring curMap; // Engine-only var
private:
    SDL_Event* m_event;
    bool m_quit;

    GameEvents* m_processor;
    AppConfig* m_appconf;

    Camera* m_camera;
    Renderer* m_renderer;
    Scene3d* m_gamescene;

    AudioManager* m_audiomgr;
    NetworkManager* m_netmgr;

    MdlLoader* m_mdlloader;
    TexLoader* m_tloader;
    MapLoader* m_maploader;

    UIManager* m_uimgr;
    //
    //
    map<unistring, unistring> m_preloadMdl, m_preloadTex, m_preloadSnd, m_preloadMus;
};

#endif // CORE_H
