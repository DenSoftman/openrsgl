#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

enum PARTICLE_MODES {
    P_RANDOM = 0,
    P_LINE = 1,
    P_CONE = 2,
    P_CONE_GRAVITY = -2,
    P_CIRCLE = 3,
    P_CIRCLE_GRAVITY = -3,
    P_EXPLOSION = 5 // Sphere
};

#include <depends.h>
#include <shader.h>

class ParticleSystem
{
public:
    ParticleSystem();

    void draw(Shader* shader);

    void setParticle(int pc, GLuint tid, int md = P_RANDOM, float mxs = 0.f, float ps = 0.f);

    glm::vec3& color();

    void render(Shader* shade, glm::vec3 pos);

private:
    GLuint pVAO, pVBO;
    GLuint pTexID;
    int pMode;
    uint_fast16_t pCount; // Particle count
    float pSize, pParticleSize;
    glm::vec3 pColor;
    glm::mat4 pMatrix;
};

#endif // PARTICLESYSTEM_H
