#ifndef GAMEEVENTS_H
#define GAMEEVENTS_H

#define OUTSIDE -1
#define INSIDE 1

class Core;

#include <camera.h>
#include <config.h>
#include <depends.h>

class GameEvents
{
public:
    GameEvents();
    glm::vec2 oldmpos;

    glm::vec2& mousePos();
    void mousePos(glm::vec2 newPos);
    bool mouseRect(int mode,
        glm::vec2 pos,
        glm::vec2 dim); // Outside/Inside test of mouse in/out of rect in "pos" with size "dim"

    bool isMouseDown(int mbtn);
    bool isMouseUp(int mbtn);
    bool isMouseClicked(int mbtn);
    bool keyDown(int scancode);

    void updateStates();

    bool isParallel(glm::vec2 first, glm::vec2 second);

    unistring mouse_state;
    bool button_down[3];
    bool button_clicked[3];

    glm::vec2 m_mpos;
};

#endif // GAMEEVENTS_H
