#ifndef MAP3D_H
#define MAP3D_H

#include <depends.h>
#include <esasl_parser.h>
#include <objectinfo.h>

class Map3d
{
public:
    Map3d();
    void addObjInfo(ObjectInfo* info);
    void attachParser(ESAsL_Parser* parser);
    void attachScript(ESAsL_Script* other);
    void setName(unistring newalias);
    void attachAudio(unistring sndn, bool looped, bool snd, bool mus);

    vector<unistring>* bgMusic();

    vector<ObjectInfo*>* objs();
    ObjectInfo* obj(int id);
    const unistring getAlias();

    void setLight(glm::vec3 pos, int type);

    const glm::vec3& getLightPos();
    int getLightType();

private:
    glm::vec3 mainLightPosition;
    int mainLightType;

    unistring m_alias;
    vector<ObjectInfo*> m_objOnMap;
    vector<ESAsL_Script*> m_scripts;
    ESAsL_Parser* m_scriptParser;

    vector<unistring> m_bgMusic;
};

#endif // MAP3D_H
