#ifndef HOOKS_H
#define HOOKS_H

#include <depends.h>
#include <logger.h>

class Core;

class Hooker
{
public:
    static int log_hook(lua_State* Ls);
    static int quit_hook(lua_State* Ls);
    static int stime_hook(lua_State* Ls);
    static int ui_addbutton_hook(lua_State *Ls);
};

#endif // HOOKS_H
