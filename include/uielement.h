#ifndef UIELEMENT_H
#define UIELEMENT_H

#include <depends.h>

class UIElement
{
public:
    virtual void scale(float factor) = 0;

    virtual void update(glm::vec2 &mousePos) = 0;

    virtual void setTex(unistring tn) = 0;
    virtual glm::vec2& pos() = 0;
    virtual int w() = 0;
    virtual int h() = 0;
    virtual int px() = 0;
    virtual int py() = 0;
    virtual const unistring &texn() = 0;

    bool isHovered, isPressed;
private:
    int width, height;
    glm::vec2 m_pos, m_dim;
    unistring m_texn;
};

#endif // UIELEMENT_H
