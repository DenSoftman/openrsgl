#ifndef OBJECTINFO_H
#define OBJECTINFO_H

#include <depends.h>

struct ObjectInfo {
    unistring o_name;
    glm::vec3 o_pos;
    glm::vec3 o_rot;
    glm::vec3 o_accel;
    glm::vec3 o_bounds;
    float o_scale;
    unistring tex;
    unistring mdl;
    int type;
    int btype;
    unistring logicType;
    unistring owner;

    bool isPlayer;
    ObjectInfo()
        : isPlayer(false)
        , o_name("")
        , type(0)
        , btype(0)
        , o_scale(1.f)
        , o_pos(glm::vec3(0.f))
        , o_rot(glm::vec3(0.f))
        , o_accel(glm::vec3(0.f))
        , o_bounds(glm::vec3(0.f))
        , tex("")
        , mdl("")
        , logicType("")
        , owner("none")
    {
    }
};

#endif // OBJECTINFO_H
