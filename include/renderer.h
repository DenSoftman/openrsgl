#ifndef RENDERER_H
#define RENDERER_H

#include <camera.h>
#include <depends.h>
#include <fontmanager.h>
#include <scene3d.h>
#include <uimanager.h>

class Core;

class Renderer // RTS-style renderer
{
public:
    Renderer();
    ~Renderer();
    void addTarget(unistring model, unistring texture);
    void render(Camera* m_cam, Scene3d* scene, UIManager* uimgr);
    void init(int width, int height);
    void initGL();
    void initSDL();

    void setWindowTitle(unistring title);
    void setWindowSize(glm::vec2 size);
    void setWindowFull(bool full);

    void grabMouse();
    void releaseMouse();
    
    SDL_Renderer* renderer();

    uint64_t stime();

    void genRenderbuffer(int w, int h, bool isDepth, GLuint& buff);
    void genTexture(int w, int h, GLuint& buff);

    glm::vec2 dim();

    GLuint gFinalFBO();

    SDL_Window* windowPtr();

    void genSkybox();
    Shader* mainShader();
    Shader* fbuffShader();

    void setLight(glm::vec3 newpos, int type = LIGHT_POINT);

private:
    glm::vec3 lpos; // Light position
    int ltype;      // Light type (LIGHT_SUN/LIGHT_POINT)

    int scrWidth, scrHeight;
    Shader *m_main_shader, *m_outline_shader, *m_fbuff_shader, *m_skybox_shader, *m_particle_shader;

    GLuint RBT, RBD; // FrameBuffer Object, RenderBuffer Texture, RenderBuffer Depth
    GLuint FBO, finalFBO, fbTexID;
    GLuint fbVAO, fbVBO; // For drawing to 'screen'
    GLuint cubemapVAO, cubemapVBO;

    SDL_Window* m_window;
    SDL_Renderer* m_iout;
    SDL_GLContext m_glcontext;

    FontManager* m_fontmgr;

    glm::vec3 lcolor; // Light color

    // Some system-vars
    uint64_t s_time; // Elapsed time
};

#endif
