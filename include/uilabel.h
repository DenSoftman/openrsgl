#ifndef UILABEL_H
#define UILABEL_H

#include <uielement.h>

class UILabel : public UIElement
{
public:
    UILabel();
    GLuint texid();
    void setTex(GLuint tid);

private:
    int width, height;
    int posX, posY;
    GLuint m_texid;
};

#endif // UILABEL_H
