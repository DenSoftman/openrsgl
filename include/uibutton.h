#ifndef UIBUTTON_H
#define UIBUTTON_H

#include <depends.h>
#include <uielement.h>

class Core;

class UIButton : public UIElement
{
public:
    UIButton();
    UIButton(glm::vec2 pos, glm::vec2 dim, unistring texn);
    UIButton(glm::vec2 pos, glm::vec2 dim, unistring texn, unistring fbind);

    void scale(float factor);
    void update(glm::vec2 &mousePos);

    GLuint texid();
    void setTex(unistring tn);
    GLuint buff();
    glm::vec2& pos();
    int w();
    int h();
    int px();
    int py();
    const unistring &texn();

    bool isHovered, isPressed;
private:
    unistring m_onActivated;
    int width, height;
    glm::vec2 m_pos, m_dim;
    unistring m_texn;
};

#endif // UIBUTTON_H
