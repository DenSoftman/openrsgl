#ifndef GENERICOBJECT3D_H
#define GENERICOBJECT3D_H

#include <depends.h>
#include <loader3d.h>
#include <shader.h>

class Core;

enum CharacterStates : uint_fast8_t {
    FREE_STATE = 0x00, // 0
    BUSY_STATE = 0x01  // 1
};

class GenericObject3d
{
public:
    GenericObject3d();
    ~GenericObject3d();
    virtual void draw(Shader* shader) = 0;

    virtual void setTex(GLuint texid) = 0;
    virtual void setVAO(GLuint vao) = 0;

    virtual void setAlias(unistring other) = 0;
    virtual const unistring& getAlias() = 0;

    virtual void move(glm::vec3 mv) = 0;
    virtual void rotate(glm::vec3 axis, float angle) = 0;
    virtual void setRotation(glm::vec3 newrot) = 0;
    virtual void scale(float factor) = 0;
    virtual void setPos(glm::vec3 newpos) = 0;
    virtual const glm::vec3& getPos() = 0;

    virtual void setModel(GLuint addr, uint sz) = 0;

    virtual void select(bool yes) = 0;
    virtual bool isSelected() = 0;
    virtual uint vertexCount() = 0;
    virtual ALuint audiosource() = 0;

    virtual void setLogicType(unistring newtype) = 0;
    virtual const unistring& getLogicType() = 0;

    virtual uint_fast8_t getState() = 0;
    virtual void setState(uint_fast8_t state) = 0;

    static uint64_t objCount;
};

#endif // GENERICOBJECT3D_H
