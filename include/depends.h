#ifndef DEPENDS_H
#define DEPENDS_H

// Object types
#define OBJ3D_STATIC 0    // Never interacted, won't be processed
#define OBJ3D_USABLE 1    // Something which will be used sometimes
#define OBJ3D_DYNAMIC 2   // Processed every event
#define OBJ3D_STREAMING 5 // Processed every frame
//

#define GAME_PORT 48684

#define TEX_SIZE 512
#define RGBC 3
#define RGBAC 4
#define VEC3_UP 0, 1, 0

#include <algorithm>
#include <array>
#include <boost/algorithm/string.hpp>
#include <boost/assign.hpp>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <locale>
#include <map>
#include <math.h>
#include <random>
#include <sstream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_net.h>

#include <lua.hpp>

#include <AL/al.h>
#include <AL/alc.h>
#include <png.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>

#undef main // SDL Windows Fix

using namespace std;
using namespace boost;

#ifndef uint
typedef unsigned int uint;
#endif
#ifndef uint32
typedef uint32_t uint32;
#endif
#ifndef uchar
typedef unsigned char uchar;
#endif

typedef std::string unistring;
typedef std::vector<unistring> unistrlist;
typedef char32_t u8char;
// ---------------------------------------------------------------------------------

#define TARGET_FPS 60
#define HEALTHBAR_HEIGHT 4

// ================ S C E N E   S T R U C T U R E =========================

#define CORRUPT_SCENE "Scene file is damaged or incomplete!"

#define CORRUPT_SCENE_NAME "'name' is damaged or incomplete!"
#define INVALID_SCENE_NAME "'name' must be STRING!"

#define CORRUPT_SCENE_AUTHOR "'author' is damaged or incomplete!"
#define INVALID_SCENE_AUTHOR "'author' must be STRING!"

#define INVALID_OBJLIST "'objects' must be OBJECT ARRAY!"
#define CORRUPT_OBJLIST "'objects' is damaged or incomplete!"

#define INVALID_COORDS "'x' and 'y' must be INT!"
#define INVALID_DIMS "'w' and 'h' must be INT!"

#define INVALID_TYPE "Object type must be STRING!"

#define INVALID_TEXS "'textures' list must be STRING ARRAY!"
#define CORRUPT_TEXS "'textures' list damaged or incomplete!"

// ========================================================================

// ============== C O N F I G   S T R U C T U R E =========================

#define CORRUPT_CONFIG "Config file is damaged or incomplete!"

// ========================================================================

#define SOUND_ACTION 0x534e44 // 'SND' in hex
#define SCENE_ACTION 0x53434e // 'SCN' in hex

#define RESIZE_ACTION 0x524553      // 'RES' in hex
#define POS_ACTION 0x504f53         // 'POS' in hex
#define POS_RES_ACTION 0xa294a6     // 'POS' + 'RES' in hex
#define RES_SND_ACTION 0xa59397     // 'SND' + 'RES' in hex
#define POS_RES_SND_ACTION 0xf2e3f9 // 'POS' + 'RES' + 'SND' in hex
#define MOV_ACTION 0x4d4f56         // 'MOV' in hex
#define SND_MOV_ACTION 0xa09d9a     // 'MOV' in hex
#define SPW_ACTION 0x535057         // 'SPW' in hex

#define SELECTED_BUILDING_ACTION 0xfb572d   // FuckingBuilding5e7ec2eD
#define DESELECTED_BUILDING_ACTION 0xfbd72d // FuckingBuildingDese7ec2eD

#define QUIT_ACTION 0x2a // Just for fun

static const GLfloat quad[] = { -1.f, +1.f, 0.f, 1.f, // Top left
    -1.f, -1.f, 0.f, 0.f,                             // Bottom left
    +1.f, -1.f, 1.f, 0.f,                             // Bottom right

    -1.f, +1.f, 0.f, 1.f,   // Top left
    +1.f, -1.f, 1.f, 0.f,   // Bottom right
    +1.f, +1.f, 1.f, 1.f }; // Top right

static float randFloat(float mx)
{
    return mx / ((rand() % 100000) / 10000);
}

static unistring vec3_2str(const glm::vec3& val)
{
    return to_string(val.x) + "," + to_string(val.y) + "," + to_string(val.z);
}

static unistring genStr(int len)
{
    unistring target;
    char* res = (char*)malloc(1 * sizeof(char));
    for(int i = 0; i < len; i++) {
        *res = (48 + (rand() % 35));
        if(*res >= 58)
            *res += 7;
        target.append(res);
    }
    free(res);
    return target;
}

static uint_fast16_t cPosFirst(unistring str, char c)
{
    for(int i = 0; i < str.size(); i++) {
        if(str[i] == c)
            return i;
    }
    return -1;
}

static uint_fast16_t cPosLast(unistring str, char c)
{
    for(int i = str.size(); i > 0; i--) {
        if(str[i] == c)
            return i;
    }
    return -1;
}

static bool notZeroVec(glm::vec3& test)
{
    if(test.x != 0.f || test.y != 0.f || test.z != 0.f)
        return true;
    return false;
}

static bool notZeroVec(glm::vec2& test)
{
    if(test.x != 0.f || test.y != 0.f)
        return true;
    return false;
}

//

static unistring nodeStrData(xmlNodePtr nd)
{
    char* data = (char*)xmlNodeGetContent(nd);
    if(data == NULL)
        return "nil";
    return unistring(data);
}

static unistring xc2str(const xmlChar *data)
{
    char* str = (char*)data;
    if(str == NULL)
        return "nil";
    else
        return unistring(str);
}

static unistring attrData(xmlNodePtr node, xmlAttrPtr attr)
{
    return xc2str(xmlGetNoNsProp(node, attr->name));
}

static unistring attrData(xmlNodePtr node, unistring attrName)
{
    return xc2str(xmlGetNoNsProp(node, (xmlChar*)(attrName.c_str())));
}

// =================++ L U A ++================================
static unistring LuaString(lua_State* L, const char* key)
{
    lua_pushstring(L, key);
    lua_gettable(L, -2);  // get table[key]

    unistring result = lua_tostring(L, -1);
    lua_pop(L, 1);  // remove string from stack
    return result;
}

static int LuaInt(lua_State* L, const char* key)
{
    lua_pushstring(L, key);
    lua_gettable(L, -2);  // get table[key]

    int result = lua_tointeger(L, -1);
    lua_pop(L, 1);  // remove int from stack
    return result;
}

#endif
