#ifndef LOADER3D_H
#define LOADER3D_H

#include <depends.h>
#include <genericobject3d.h>
#include <logger.h>

class Loader3D
{
public:
    Loader3D();
    ~Loader3D();

    void LoadModelDAE(unistring fname, vector<GLfloat>* vertexData);
    void LoadModelOBJ(unistring fname, vector<GLfloat> *vertexData);
};

#endif
