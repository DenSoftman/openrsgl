#ifndef MAPLOADER_H
#define MAPLOADER_H

#include <depends.h>
#include <logger.h>
#include <map3d.h>

class Core;

class MapLoader
{
public:
    MapLoader();

    void loadMap(unistring mf,
        unistring ma,
        map<unistring, unistring>* mdls = NULL,
        map<unistring, unistring>* texs = NULL,
        map<unistring, unistring>* sounds = NULL,
        map<unistring, unistring>* music = NULL);
    Map3d* mapPtr(unistring ma);

private:
    map<unistring, Map3d*> a_maps;
    xmlDocPtr curDoc;
    xmlNodePtr curNode;
};

#endif // MAPLOADER_H
