#ifndef SHADER_H
#define SHADER_H

#include <depends.h>

class Shader
{
public:
    Shader(unistring vs, unistring fs, unistring gs = "none");
    GLuint ID;
    void use();

    void setMat4(unistring id, glm::mat4& matrix);
    void setInt(unistring id, int value);
    void setFloat(unistring id, float value);
    void setVec3(unistring id, glm::vec3& vec);
    void setVec2(unistring id, glm::vec2& vec);

    void loadShader(unistring fvname, unistring ffname, unistring fgname = "none");
};

#endif
