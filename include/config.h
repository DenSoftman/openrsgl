#ifndef CONFIG_H
#define CONFIG_H

// -----------=
#define DEBUG
// -----------=

#define LIGHT_SUN 1
#define LIGHT_POINT 0

#define GLM_FORCE_RADIANS

#define CONSOLE_KEY SDL_SCANCODE_GRAVE

#define DW_WIDTH 1280
#define DW_HEIGHT 720
#define DW_FULL false

#include <depends.h>
#include <logger.h>

#define SCROLL_SPEED 5.f
#define SCROLL_BORDER 80

#define RES_ROOT "data/"
#define CFG_FILE RES_ROOT "config.lua"

#define IMG_ROOT RES_ROOT "img/"
#define AUDIO_ROOT RES_ROOT "audio/"
#define SOUND_ROOT AUDIO_ROOT "sounds/"
#define MUSIC_ROOT AUDIO_ROOT "music/"
#define SCRIPTS_ROOT RES_ROOT "scripts/"
#define SCENE_ROOT RES_ROOT "scenes/"
#define SHADERS_ROOT RES_ROOT "shaders/"
#define MODELS_ROOT RES_ROOT "models/"
#define MAPS_ROOT RES_ROOT "maps/"

#define DELTATIME ((float)1.f / TARGET_FPS)

class Core;

struct AppConfig {
    AppConfig():
        app_width(0), app_height(0), app_isfull(false)
    {
    }
    unistring app_name;
    uint16_t app_width, app_height;
    bool app_isfull;

    map<unistring, unistring> app_textures;
    map<unistring, unistring> app_models;
    map<unistring, int> anim_fps;

    unistring start_scene, start_script;
    map<unistring, unistring> sound_files;
    map<unistring, unistring> music_files;

    void setStartScene(unistring scene)
    {
        start_scene = scene;
    }

    unistring getStartScene()
    {
        return start_scene;
    }
};

class Config
{
public:
    static void cfgerr(unistring errmsg);
    static void loadCfg(AppConfig *conf);
    static void cfgwarn(unistring warnmsg);
};
#endif // CONFIG_H
