#ifndef ORSGL_WORKER_H
#define ORSGL_WORKER_H

#include <depends.h>
#include <object3d.h>

/*
 * CharacterInfo struct defines info about each peasent in game
 */
struct CharacterInfo // No need to write 'typedef CharacterInfo' here
{
    // UTF-8 encoded names allowed, example: (Василий Петрович, Max Schübert, 보라 대중, 大中)
    unistring Name, Lastname; //  Name, Lastname for this character, example: Harry Potter (Name:Harry, Lastname:Potter)

    // unsigned 16-bit integer allows up to 65536 jobs
    uint_fast16_t
        Job; // Simply id of job applied for this character, example: (uint16_t)(ID_WOODPECKER, ID_MINER, ID_POLICEMAN)

    // unsigned 8-bit integer allows 0-255
    uint_fast8_t Age; // No need to use more, age is 0-100 usually
};

class ORSGL_Worker : public Object3d
{
public:
    ORSGL_Worker();
    ORSGL_Worker(CharacterInfo* info);

private:
    CharacterInfo* m_cinfo;
};

#endif // ORSGL_WORKER_H
