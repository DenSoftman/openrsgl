#ifndef UIMANAGER_H
#define UIMANAGER_H

#include <config.h>
#include <depends.h>
#include <logger.h>
#include <shader.h>

#include <uibutton.h>
#include <uilabel.h>

class Core;

class UIManager
{
public:
    UIManager(int w, int h);
    ~UIManager();

    void clear();

    void render(GLuint targetFBO);
    unistring addElement(UIElement *other);
    bool isOver(unistring item);
private:
    glm::vec2 uiDimension;
    map<unistring, UIElement*> m_elements;

    Shader* m_uishader;
    GLuint quadVAO, quadVBO;

    static int uiElementID;
};

#endif /* UIMANAGER_H */
