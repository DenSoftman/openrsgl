#ifndef ESASL_PARSER_H
#define ESASL_PARSER_H

#include <depends.h>
#include <logger.h>

struct ESAsL_Func {
    unistring func;
    unistrlist arguments;

    ESAsL_Func(unistring f)
        : func(f)
    {
    }

    ESAsL_Func(unistring f, unistrlist args)
        : func(f)
        , arguments(args)
    {
    }
};

struct ESAsL_Script {
    unistring name;
    vector<ESAsL_Func*> funcs;
    map<unistring, unistring> variables;

    void appendFunc(ESAsL_Func* func);
    void exec();
};

class ESAsL_Parser
{
public:
    ESAsL_Parser();
    ESAsL_Script* parseText(unistring text);
    ESAsL_Script* parseFile(unistring fname);
    void parseLine(unistring line, ESAsL_Script* tscript);

    static unistring solveMath(unistring eval);
};

#endif // ESASL_PARSER_H
