#ifndef ORSGL_STRUCTURE_H
#define ORSGL_STRUCTURE_H

#include <depends.h>
#include <object3d.h>

class ORSGL_Structure : public Object3d
{
public:
    ORSGL_Structure();

private:
    unistring structType; // Main, Producing, Defensive
};

#endif // ORSGL_STRUCTURE_H
