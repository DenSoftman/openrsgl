#ifndef TEXLOADER_H
#define TEXLOADER_H

#include <depends.h>
#include <logger.h>

enum FAILS { FAILED_NOFILE = -1, FAILED_FREAD = -2, FAILED_FINFO = -3 };

struct Texture {
    uint32 width, height, depth;
    GLubyte* data;

    Texture(uint w, uint h, GLubyte* d)
        : width(w)
        , height(h)
        , data(d)
    {
    }

    Texture()
        : width(0)
        , height(0)
        , data(nullptr)
    {
    }

    int loadTex(unistring fname, bool cubemap = false);
    void unloadTex()
    {
        width, height = 0;
        if(data)
            free(data);
    }
};

class TexLoader
{
public:
    TexLoader();
    ~TexLoader();

    void loadTexture(unistring tf, unistring alias);
    void loadTexture(unistring tf, unistring alias, int width, int height);

    // GLuint loadTexture(unistring tf);
    void loadCubemap(vector<unistring> cbf, int id);
    void getTextureInfo(unistring alias, GLuint* texid);
    GLuint texid(unistring alias);
    GLuint cubemap(int id);

private:
    Texture tmptex;
    map<unistring, GLuint> m_texids;
    vector<GLuint> m_cubemaps;
};

#endif // TEXLOADER_H
