#ifndef SCENE3D_H
#define SCENE3D_H

#include <depends.h>
#include <object3d.h>
#include <particlesystem.h>

class Scene3d
{
public:
    Scene3d();
    ~Scene3d();
    const map<unistring, GenericObject3d*>& objs();
    GenericObject3d* obj(unistring alias);

    const map<unistring, ParticleSystem*>& particleSys();
    ParticleSystem* psys(unistring alias);

    void setParticle(unistring alias, int pc, int md = P_RANDOM, float mxs = 0, float ps = 0);

    void moveObj(unistring alias, glm::vec3 mvmnt);
    void rotateObj(unistring alias, glm::vec3 axis, float angle);
    void setObjRotation(unistring alias, glm::vec3 newRotation);
    void resizeObj(unistring alias, float factor);

    const glm::vec3& getObjPos(unistring alias);

    void select(unistring i);
    void deselect(unistring i);

    void addObject(GLuint addr,
        uint sz,
        GLuint tid,
        unistring alias,
        bool spawn = false,
        glm::vec3 initPos = glm::vec3(0.f),
        glm::vec3 initRot = glm::vec3(0.f),
        glm::vec3 accel = glm::vec3(0.f),
        glm::vec3 bounds = glm::vec3(0.f),
        int bt = 0,
        unistring logicType = "none",
        float scale = 1.f);
    void addPSys(unistring alias, int mode = P_RANDOM);
    void remove(GenericObject3d* tobj);

    void attachObject(GenericObject3d* gobj);

    void updateScene();

private:
    void del(GenericObject3d* target);
    map<unistring, GenericObject3d*> m_objects;
    map<unistring, ParticleSystem*> m_pSystems;
    //
    vector<GenericObject3d*> toDelete;
    vector<GenericObject3d*> toAdd;
};

#endif // SCENE3D_H
